<?php

/*
 * Fetch bug list from GitHub issue tracker and pretty-print it into a
 * HTML file.
 *
 * Usage: php genissues.php
 */
define(GITHUB_USER, 'Koura');
define(GITHUB_REPO, 'CogFut');
define(OUTPUT_FILE, 'issues.html');

$out = fopen(OUTPUT_FILE, "w");
$curl = curl_init();
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

function parse($url) {
    global $curl;
    curl_setopt($curl, CURLOPT_URL, $url);
    $resp = curl_exec($curl);
    if (!$resp) die(curl_error($curl).' fetching '.$url);
    return json_decode($resp);
}

function paratext($s) {
    $s = str_replace("\n", "<br/>", $s);
    return $s;
}

fwrite($out, '<meta charset="utf-8"/>');
fwrite($out, '<body style="font-family:sans-serif; max-width:40em;">');
fwrite($out, '<h1>Avoimet bugit</h1>');
foreach (parse("https://api.github.com/repos/".GITHUB_USER."/".GITHUB_REPO."/issues") as $issue) {
    fwrite($out, '<h2>'.$issue->title.'</h2>');
    fwrite($out, '<p>'.paratext($issue->body).'</p>');
    if ($issue->comments_url) {
        foreach (parse($issue->comments_url) as $comment) {
            fwrite($out, '<p><b>Kommentti:</b> '.paratext($comment->body).'</p>');
        }
    }
}
fwrite($out, '</body>');

curl_close($curl);
