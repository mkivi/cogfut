#! /bin/sh
#
# Generate PNG images of characters into the current directory using ImageMagick.
#
set -e # Exit on error
generate() {
    color="$1"; char="$2"
    convert -size 50x50 xc:"$color" -pointsize 20 -gravity center -draw "text 0,0 \"$char\"" -colors 16 "char$color$char".png
}
generate Red 1
generate Red 2
generate Blue P
