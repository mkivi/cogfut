require.config({
    baseUrl: '../../src/main/webapp/js/',
    urlArgs: 'cb=' + Math.random(), // Add random string to URL to prevent caching
    paths: {
        bootstrap      : 'libs/bootstrap/js/bootstrap',
        jquery         : 'libs/jquery/jquery-1.8.2',
        kinetic        : 'libs/kinetic-js/kinetic-v4.7.2',

        jasmine        : '../../../../tests/jasmine/lib/jasmine-1.3.0/jasmine',
        'jasmine-html' : '../../../../tests/jasmine/lib/jasmine-1.3.0/jasmine-html',
        spec           : '../../../../tests/jasmine/spec'
    },
    shim: {
        bootstrap: {
            deps: ['jquery']
        },
        jasmine: {
            exports: 'jasmine'
        },
        'jasmine-html': {
            deps: ['jasmine'],
            exports: 'jasmine'
        },
        kinetic: {
            deps: [],
            exports: 'Kinetic'
        }
    }
});

window.store = "TestStore"; // override local storage store name - for testing

require(['jquery', 'jasmine-html'], function ($, jasmine) {
    var htmlReporter, specs;
    htmlReporter = new jasmine.HtmlReporter();
    jasmine.getEnv().updateInterval = 1000;
    jasmine.getEnv().addReporter(htmlReporter);
    jasmine.getEnv().specFilter = function (spec) {
        return htmlReporter.specFilter(spec);
    };
    specs = [];
    specs.push('spec/LevelSpec');
    specs.push('spec/MotionSpec');
    specs.push('spec/PointSpec');
    $(function () {
        require(specs, function () {
            jasmine.getEnv().execute();
        });
    });
});
