define(function(require) {

    var $      = require('jquery');
    var Assets = require('assets');
    var CogFut = require('cogfut');
    var Const  = require('const');
    var Level  = require('level');
    var Level0 = require('level0');
    var Motion = require('motion');
    var Point  = require('point');

    describe('Level', function() {

        beforeEach(function() {
            Assets.basePath = '../../src/main/webapp/';
            this.container = $('<div/>', {
                id: 'cogfut-container',
                // Container needs to have visibility:hidden rather
                // than display:none. Otherwise HTML5 canvas stuff
                // doesn't work, at least in Chrome.
                style: 'width:300px; height:300px; visibility:hidden;'
            });
            this.container.appendTo($('body'));
            this.cogfut = new CogFut();
        });

        afterEach(function() {
            this.container.remove();
        });

        describe('Initialization', function() {

            it('should bring up Level0 correctly', function() {
                this.cogfut.playLevel(0);
                waitsFor(function() { return (this.cogfut.level instanceof Level); }, 'level to load', 10000);
                runs(function() {
                    var level;
                    level = this.cogfut.level;
                    expect(level.playerPawnList.length).toEqual(2);
                    expect(level.aiPawnList.length).toEqual(0);
                });
            });

            it('should bring up Level1 correctly', function() {
                this.cogfut.playLevel(1);
                waitsFor(function() { return (this.cogfut.level instanceof Level); }, 'level to load', 10000);
                runs(function() {
                    var level;
                    level = this.cogfut.level;
                    expect(level.playerPawnList.length).toEqual(2);
                    expect(level.playerPawnList[0].point).toEqual(new Point(Const.fieldCenterPointX-5, Const.fieldCenterPointY-15));
                    expect(level.playerPawnList[1].point).toEqual(new Point(Const.fieldCenterPointX+5, Const.fieldCenterPointY-15));
                    expect(level.aiPawnList.length).toEqual(1);
                    expect(level.aiPawnList[0].point).toEqual(new Point(Const.fieldCenterPointX, Const.fieldCenterPointY-40));
                    expect(level.ball.turn.source).toEqual(level.playerPawnList[0]);
                });
            });

        });

        describe('Basic game mechanics', function() {

            it('should implement passing and running properly', function() {
                var level, pawn1, pawn1End, ballEnd;
                this.cogfut.playLevel(1);
                waitsFor(function() { return (this.cogfut.level instanceof Level); }, 'level to load', 10000);
                runs(function() {
                    level = this.cogfut.level;
                    pawn1 = level.playerPawnList[0];
                    pawn1End = Point.addPoints(pawn1.point, new Point(+5, -5));
                    ballEnd  = Point.addPoints(pawn1.point, new Point(-5, -5));
                    pawn1.turn.targetPoint = pawn1End;
                    level.ball.setActionToPass(ballEnd);
                    level.playTurn();
                });
                waitsFor(function() { return !level.anim; }, 'turn to finish', 10000);
                runs(function() {
                    expect(Point.distanceBetween(pawn1.point, pawn1End)).toBeLessThan(0.5);
                    expect(Point.distanceBetween(level.ball.point, ballEnd)).toBeLessThan(0.5);
                    expect(level.ball.turn.source).toEqual(level.ball.point);
                });
            });

        });

        describe('AI', function() {

            it('should intercept a pass properly', function() {
                var level, pawn1, pawn2, pawn1End, pawn2End, failMsg;
                this.cogfut.playLevel(1);
                waitsFor(function() { return (this.cogfut.level instanceof Level); }, 'level to load', 10000);
                runs(function() {
                    level = this.cogfut.level;
                    pawn1 = level.playerPawnList[0];
                    pawn2 = level.playerPawnList[1];
                    pawn1End = Point.addPoints(pawn1.point, new Point(0, -Const.pawnMoveRangeWithBall));
                    pawn2End = Point.addPoints(pawn2.point, new Point(0, -Const.pawnMoveRangeWithBall));
                    pawn1.turn.targetPoint = pawn1End;
                    pawn2.turn.targetPoint = pawn2End;
                    level.playTurn();
                });
                waitsFor(function() { return !level.anim; }, 'turn to finish', 10000);
                runs(function() {
                    expect(level.ball.turn.source).toEqual(pawn1);
                    expect(Point.distanceBetween(pawn1.point, pawn1End)).toBeLessThan(0.2);
                    expect(Point.distanceBetween(pawn2.point, pawn2End)).toBeLessThan(0.2);
                    level.ball.setActionToPass(pawn2.point);
                    failMsg = undefined;
                    this.cogfut.onLevelFinished(function(newFailMsg) { failMsg = newFailMsg; });
                    level.playTurn();
                });
                waitsFor(function() { return (failMsg !== undefined); }, 'game to end', 10000);
                runs(function() {
                    expect(failMsg).toEqual('vastustaja sai pallon');
                });
            });

        });

    });

});
