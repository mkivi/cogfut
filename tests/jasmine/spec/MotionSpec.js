define(function(require) {

    var Motion = require('motion');
    var Point  = require('point');

    describe('Motion', function () {

        describe('Initializes correctly', function () {

            it('should set point, point0 and point1 properties correctly', function () {
                var p = new Point(1.5, 3.6);
                var q = new Point(6.5, 4.2);
                var s = 3;
                var m = new Motion(p, q, s);
                expect(m.point).toEqual(p);
                expect(m.point0).toEqual(p);
                expect(m.point1).toEqual(q);
            });

        });

        describe('Updates correctly', function () {

            it('should increment point correctly along a horizontal line', function () {
                var p = new Point(1, 0);
                var q = new Point(3, 0);
                var s = 1;
                var m = new Motion(p, q, s);
                expect(m.point).toEqual(new Point(1, 0));
                expect(m.updateForFrame(1)).toEqual(true);
                expect(m.point).toEqual(new Point(1, 0));
                expect(m.updateForFrame(2)).toEqual(true);
                expect(m.point).toEqual(new Point(2, 0));
                expect(m.updateForFrame(3)).toEqual(true);
                expect(m.point).toEqual(new Point(3, 0));
                expect(m.updateForFrame(4)).toEqual(false);
                expect(m.point).toEqual(new Point(3, 0));
            });

            it('should increment point correctly along a vertical line', function () {
                var p = new Point(1, 1);
                var q = new Point(1, 3);
                var s = 1;
                var m = new Motion(p, q, s);
                expect(m.point).toEqual(new Point(1, 1));
                expect(m.updateForFrame(1)).toEqual(true);
                expect(m.point).toEqual(new Point(1, 1));
                expect(m.updateForFrame(2)).toEqual(true);
                expect(m.point).toEqual(new Point(1, 2));
                expect(m.updateForFrame(3)).toEqual(true);
                expect(m.point).toEqual(new Point(1, 3));
                expect(m.updateForFrame(4)).toEqual(false);
                expect(m.point).toEqual(new Point(1, 3));
            });


            it('should increment point correctly along a diagonal line', function () {
                var p = new Point(1, 1);
                var q = new Point(3, 3);
                var s = 1;
                var m = new Motion(p, q, s);
                expect(m.point).toEqual(new Point(1, 1));
                expect(m.updateForFrame(1)).toEqual(true);
                expect(m.point).toEqual(new Point(1, 1));
                expect(m.updateForFrame(2)).toEqual(true);
                expect(Point.distanceBetween(m.point, new Point(1.707, 1.707))).toBeLessThan(0.01);
                expect(m.updateForFrame(3)).toEqual(true);
                expect(Point.distanceBetween(m.point, new Point(2.414, 2.414))).toBeLessThan(0.01);
                expect(m.updateForFrame(4)).toEqual(false);
                expect(Point.distanceBetween(m.point, new Point(2.414, 2.414))).toBeLessThan(0.01);
            });


        });

    });

});
