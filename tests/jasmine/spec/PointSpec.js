define(function(require) {

    var Point   = require('point');
    var Kinetic = require('kinetic');
    var Const   = require('const');

    describe('Point', function () {
        Point.pixelsPerUnit = 2;

        describe('Initializes correctly', function () {

            it('should set x and y correctly', function () {
                var p = new Point(1.5, 3.6);
                expect(p.x).toEqual(1.5);
                expect(p.y).toEqual(3.6);
            });

        });

        describe('Does vector math correctly', function () {

            it('should calculate dot product correctly', function () {
                expect(Point.dot(new Point(1,2), new Point(3,4))).toEqual(11);
            });

        });

        describe('Does conversion between units and pixels correctly', function () {

            it('should convert pixels to units correctly', function () {
                expect(Point.unitsFromPixels(6)).toEqual(3);
            });

            it('should convert units to pixels correctly', function () {
                expect(Point.pixelsFromUnits(3)).toEqual(6);
            });
        });

        describe('Makes a point correctly', function () {

            it('should make a point from pixels correctly', function () {
                expect(Point.fromPixels(40, 40)).toEqual(new Point(20,20));
            });

            it('should make a point from shape correctly', function () {
                var shape = new Kinetic.RegularPolygon(Const.pawnMainShapeStyle);
                shape.setX(20);
                shape.setY(10);
                expect(Point.fromShape(shape)).toEqual(new Point(10,5));
            });
        });

        describe('Does xy pixel array from points correctly', function () {

            it('should make an array from 1 point correctly', function () {
                expect(Point.xyPixelArrayFromPoints(new Point(2,2))).toEqual([4,4]);
            });

            it('should make an array from 3 points correctly', function () {
                expect(Point.xyPixelArrayFromPoints(new Point(2,2), new Point(3,3), new Point(4,4))).toEqual([4,4,6,6,8,8]);
            });
        });

        describe('Does calculations between points correctly', function () {

            it('should add second point to the first correctly', function () {
                expect(Point.addPoints(new Point(2,2), new Point(4,4))).toEqual(new Point(6,6));
            });

            it('should substract first point from the second correctly', function () {
                expect(Point.subtract(new Point(5,7), new Point(4,3))).toEqual(new Point(1,4));
            });

            it('should calculate distance between points correctly', function () {
                expect(Point.distanceBetween(new Point(2,7), new Point(10,12))).toBeCloseTo(9.43);
            });

            it('should calculate angle between points correctly', function () {
                expect(Point.angleBetween(new Point(5,7), new Point(8,12))).toBeCloseTo(1.03);
            });
        });

        describe('Moves a point correctly', function () {

            it('should move a point to predefined direction correctly', function () {
                var p1, p2, p3;
                p1 = new Point(8,10);
                p2 = new Point(6.692,8.486);
                expect(Point.distanceBetween(p1.nUnitsInDirection(4,2), p2)).toBeLessThan(0.01);
            });

            it('should move the point towards the other point correctly', function () {
                var p1, p2, p3;
                p1 = new Point(9,9);
                p2 = new Point(1,1);
                p3 = new Point(7.586,7.586);
                expect(Point.distanceBetween(p1.nUnitsToward(p2, 2), p3)).toBeLessThan(0.01);
            });
        });

        describe('Other', function () {

            it('should clone the point', function () {
                var p1;
                p1 = new Point(8,10);
                expect(p1.clone()).toEqual(new Point(8,10));
            });

            it('same points should be the same', function () {
                var p1, p2, p3;
                p1 = new Point(9,9);
                p2 = new Point(9,9);
                p3 = new Point(8.8,9);
                expect(p1.equals(p2)).toEqual(true);
                expect(p1.equals(p3)).toEqual(false);
            });
        });
    });

});
