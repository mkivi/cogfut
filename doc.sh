#! /bin/sh
#
# Generate JsDoc and JavaDoc documentation
#

set -e # Exit on error
cd "$(dirname "$0")" # Change to script directory
root="$(pwd -P)" # Save it for later use

cd "$root"
test -d doc || mkdir doc

cd "$root"/src/main/webapp/js
out="../../../../doc/jsdoc"
test -e "$out" && rm -rf "$out"
jsd="../../../../tools/jsdoc-toolkit"
java -jar "$jsd"/jsrun.jar "$jsd"/app/run.js -a -p -d="$out" -e=utf-8 -t="$jsd"/templates/jsdoc *.js 1>&2

cd "$root"/src/main/java/cogfut
out="../../../../doc/javadoc"
test -e "$out" && rm -rf "$out"
find . -type f -iname "*.java" | xargs javadoc -d "$out"
