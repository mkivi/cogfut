#! /bin/sh
#
# Create distribution packages in distfiles/ directory
#

set -e  # Exit on error
cd "$(dirname "$0")" # Change to script directory
root="$(pwd -P)" # Save it for later use
test -d distfiles || mkdir distfiles
pkga="distfiles/cogfut-asiakas"
pkgd="distfiles/cogfut-devaaja"

cd "$root"
test -e "$pkga".zip && rm -f "$pkga".zip
cd src/main/webapp/
find "." -type f -print | grep -v WEB-INF | grep -v "/\\." | zip -q ../../../"$pkga".zip -@

# TODO: somehow exclude files mentioned in .gitignore
cd "$root"
test -e "$pkgd".tgz && rm -f "$pkgd".tgz
tar -czf "$pkgd".tgz --exclude distfiles .
