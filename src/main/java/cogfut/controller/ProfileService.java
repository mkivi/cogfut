package cogfut.controller;

import cogfut.domain.Account;
import cogfut.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProfileService {

    @Autowired
    private AccountService accountService;
    
    /**
     * Listens to GET requests from path $contextPath/api/profile/insert_username_here.
     * Returns the requested account if such is found. Password is not visible in the 
     * returned account. If the account is not found, null is returned.
     * @param userId
     * @return 
     */
    @RequestMapping(value = "profile/{userId}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Account profileInfo(@PathVariable(value = "userId") String userId) {
        if (accountService.exists(userId)) {
            Account acc = accountService.read(userId);
            Account profile = new Account();
            profile.setUsername(acc.getUsername());
            profile.setGames(acc.getGames());
            profile.setWon(acc.getWon());
            profile.setLost(acc.getLost());
            return profile;
        }
        return null;
    }
    /**
     * Listens to POST requests from path $contextPath/api/profile/insert_username_here.
     * Games played and won are incremented for the user that is given in the request and
     * the new values are updated in the database.
     * @param userId 
     */
    @RequestMapping(value = "won/{userId}", method = RequestMethod.POST) 
    @ResponseBody
    public void incrementGamesWon(@PathVariable(value = "userId") String userId) {
        if(accountService.exists(userId)) {
            Account acc = accountService.read(userId);
            acc.addGamesPlayed();
            acc.addGamesWon();
            accountService.update(acc);
        }
    }
    
    /**
     * Listens to POST requests from path $contextPath/api/profile/insert_username_here.
     * Games played and lost are incremented for the user that is given in the request and
     * the new values are updated in the database.
     * @param userId 
     */
    @RequestMapping(value = "lost/{userId}", method = RequestMethod.POST)
    @ResponseBody
    public void incrementGamesLost(@PathVariable(value = "userId") String userId) {
        if(accountService.exists(userId)) {
            Account acc = accountService.read(userId);
            acc.addGamesPlayed();
            acc.addGamesLost();
            accountService.update(acc);
        }
    }
}
