package cogfut.controller;

import cogfut.domain.Account;
import cogfut.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginService {

    @Autowired
    private AccountService accountService;
    /**
     * Listens to POST requests from address $contextPath/api/login and requires that username and password
     * are given in the request. Checks if an account exists for the given username
     * and then checks if the password is valid for the account. Login fails
     * if the password and username don't match.
     * @param username
     * @param password
     * @return boolean
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public @ResponseBody boolean login(@RequestParam(required=true) String username, @RequestParam(required=true) String password) {
        Account acc = accountService.read(username);
        if(acc!=null && acc.getPassword().equals(password)) {
                return true;           
        }
        return false;
    }
    /**
     * Listens to POST requests from address in path $contextPath/api/register.
     * If an account with the given username doesn't exist, 
     * a new account is created.
     * @param username
     * @param password
     * @param age
     * @return 
     */
    @RequestMapping(value = "register", method = RequestMethod.POST)
    public @ResponseBody boolean createAccount(@RequestParam String username,
                                 @RequestParam String password,
                                 @RequestParam int age) {
        if (!accountService.exists(username)) {
            Account account = new Account();
            account.setUsername(username);
            account.setPassword(password);
            account.setGames(0);
            account.setLost(0);
            account.setWon(0);
            accountService.create(account);
            return true;
        }
        return false;
    }
    /*
     *\o/
     */
    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public @ResponseBody boolean logout() {
        return true;
    }
}