
package cogfut.repository;

import cogfut.domain.Account;
import org.springframework.data.repository.CrudRepository;
/**
 * Check Spring CrudRepository documentation.
 * @author koura
 */
public interface AccountRepository extends CrudRepository<Account, String> {
    
}
