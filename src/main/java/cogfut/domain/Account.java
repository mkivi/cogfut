package cogfut.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class that is saved in the database with table name Account.
 * The database table contains columns: username, password, games played, games
 * won and games lost.
 * @author koura
 */
@Entity
@Table(name = "Account")
public class Account implements Serializable {

    @Id
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "games")
    private long games;
    @Column(name = "won")
    private long won;
    @Column(name = "lost")
    private long lost;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public long getGames() {
        return games;
    }

    public void setGames(long games) {
        this.games = games;
    }

    public long getWon() {
        return won;
    }

    public void setWon(long won) {
        this.won = won;
    }

    public long getLost() {
        return lost;
    }

    public void setLost(long lost) {
        this.lost = lost;
    }
    /**
     * Increments games played.
     */
    public void addGamesPlayed() {
        this.games++;
    }
    /**
     * Increments games won.
     */
    public void addGamesWon() {
        this.won++;
    }
    /**
     * Increments games lost.
     */
    public void addGamesLost() {
        this.lost++;
    }
}
