
package cogfut.service;

import cogfut.domain.Account;

public interface AccountService {
    Account create(Account player);
    Account read(String identifier);
    void delete(String identifier);
    boolean exists(String identifier);
    Account update(Account acc);
   
}
