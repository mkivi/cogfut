/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cogfut.service;

import cogfut.domain.Account;
import cogfut.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * If you don't know what's happening go check spring tutorials 
 * for annotations @Service, @Transactional, @Autowired etc.
 * Stuff is deleted, searched and saved in/from the database.
 * @author koura
 */
@Service
public class JpaAccountService implements AccountService{

    @Autowired
    AccountRepository accountRepository;

    @Override
    @Transactional(readOnly = false)
    public Account create(Account account) {
        return accountRepository.save(account);
    }

    @Override
    @Transactional(readOnly = true)
    public Account read(String identifier) {
         return accountRepository.findOne(identifier);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(String identifier) {
        accountRepository.delete(identifier);
    }
    
    @Override 
    @Transactional(readOnly = true)
    public boolean exists(String identifier) {
        return accountRepository.exists(identifier);
    }
    @Override
    @Transactional(readOnly = false)
    public Account update(Account acc) {
        return create(acc);
    }
    
}
