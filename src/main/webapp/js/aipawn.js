define(function (require) {

    var Kinetic = require('kinetic');

    var Assets  = require('assets');
    var Const   = require('const');
    var Motion  = require('motion');
    var Pawn    = require('pawn');
    var Point   = require('point');

    /**
     * @name AIPawn
     *
     * @class Represents an AI-controlled player on the opposing team. 
     * Subclass of the Pawn class.
     *
     * @property {Kinetic.Image} mainShape
     * @property {Kinetic.Circle} ghostRange
     *
     * @constructor
     * @param {Level} level The level object that commissions us. We can access the Kinetic stage and all other game objects through this.
     * @param {Point} initialOffset The location of the pawn at the beginning of the level.
     */
    function AIPawn(level, initialOffset) {
        this.super = this.__proto__.__proto__;
        this.super.constructor.call(this, level, initialOffset);
        this.mainShape = new Kinetic.RegularPolygon(Const.pawnMainShapeStyle);
        this.mainShape.setFill(Assets.getImageAttrs(Const.aiPawnImages[this.level.aiPawnList.length]));
        this.mainShape.setRotation(Math.PI);
        this.mainShape.moveToBottom();
        this.mainShape.on('mouseover', this.mainShapeMouseOver.bind(this));
        this.mainShape.on('mouseout', this.mainShapeMouseOut.bind(this));
        this.ghostRange = new Kinetic.Circle(Const.aiMoveRangeStyle);
        this.ghostRange.setRadius(Point.pixelsFromUnits(Const.aiPawnMoveRange));
        this.ghostRange.setVisible(false);
        this.level.mainLayer.add(this.ghostRange);
        this.level.mainLayer.add(this.mainShape);
        this.level.aiPawnList.push(this);
    }

    AIPawn.prototype.__proto__ = Pawn.prototype;

    /**
     * Shows AIPawn's move range when hovering mouse over
     */
    AIPawn.prototype.mainShapeMouseOver = function () {
        this.ghostRange.show();
        this.ghostRange.moveToBottom();
        this.level.mainLayer.draw();
    }

    /**
     * Hides AIPawn's move range when hovering mouse out
     */
    AIPawn.prototype.mainShapeMouseOut = function () {
        this.ghostRange.hide();
        this.level.mainLayer.draw();
    }

    /**
     * This method is called at the beginning of the "setup phase" of
     * each turn. The setup phase is the phase where you can move the
     * pawns and the ball, etc. The setup phase ends when the "Play"
     * button is clicked.
     * 
     * @param {number} turnIndex The zero-based index of the turn that is beginning
     */
    AIPawn.prototype.beginTurnSetup = function (turnIndex) {
        this.turn = this.turnList[turnIndex];
        this.setPoint(this.turn.sourcePoint);
    }

    /**
     * This method is called at the end of the "setup phase" of each
     * turn, just before the animation is started. 
     */
    AIPawn.prototype.endTurnSetup = function () {
        switch (this.level.ball.turn.action.type) {
        case 'shot':
        case 'pass':
            this.level.ball.interceptedAt = this.calcPassInterceptionPoint();
            if (this.level.ball.interceptedAt) {
                this.turn.targetPoint = this.level.ball.interceptedAt.clone();
            } else {
                this.turn.targetPoint = this.calcNewPoint(this.level.ball.turn.action.targetPoint);
            }
            break;
        case 'wallpass':
            this.turn.targetPoint = this.calcNewPoint(this.level.ball.turn.action.wallPawn.calcBallTargetPoint());
            break;
        case null:
            if (this.level.ball.turn.source instanceof Point) {
                this.turn.targetPoint = this.calcNewPoint(this.level.ball.turn.source);
            } else { // Dribble
                this.turn.targetPoint = this.calcNewPoint(this.level.ball.turn.source.calcBallTargetPoint());
            }
            break;
        }
        this.motion = new Motion(this.turn.sourcePoint, this.turn.targetPoint, Const.pawnSpeedWithoutBall);
    }

    /**
     * This method is called once for each animation frame while a
     * turn is being animated. It computes the position of the object
     * during that frame and moves the object to that position on the
     * screen as well. It returns true if the object may still move,
     * or false if the object has reached the end of its motion for
     * this turn.
     *
     * @param {number} seconds Kinetic frame's starting seconds
     * @return {boolean} Are we still moving
     */
    AIPawn.prototype.updateForFrame = function (seconds) {
        if (this.level.ball.turn.shield && (Point.distanceBetween(this.point, this.level.ball.point) < 5)) {
            this.turn.targetPoint = this.point;
            this.motion = null;
            return false;
        }
        if (this.motion === null) {
            return false;
        }
        if (!this.motion.updateForFrame(seconds)) {
            return false;
        }
        this.setPoint(this.motion.point);
        this.mainShape.setRotation(this.motion.angle);
        return true;
    }

    /**
     * Sets point for Pawn
     * @param {Point} newPoint
     */
    AIPawn.prototype.setPoint = function (newPoint) {
        this.point = newPoint.clone();
        this.point.assignToShape(this.mainShape);
        this.point.assignToShape(this.ghostRange);
    }

    /**
     * Calculates new x and y point so that myPoint moves
     * Const.aiPawnMoveRange amount towards targetPoint. Returns the
     * new calculated point.
     *
     * @private
     * @param {Point} targetPoint Point where we're heading
     * @return {Point} New target point
     */
    AIPawn.prototype.calcNewPoint = function (targetPoint) {
        return this.point.nUnitsToward(targetPoint, Const.aiPawnMoveRange);
    }

    /**
     * Check whether the ball will go through our moverange during
     * this turn. If so, return the interception point inside the
     * moverange where we can catch the ball. Otherwise return null,
     * meaning we won't be able to catch the ball this turn.
     *
     * @private
     * @return {Point|null} Point if we are able to catch the ball, otherwise null.
     */
    AIPawn.prototype.calcPassInterceptionPoint = function () {
        var aiStart, ballStart, ballEnd;
        var d, f, r, a, b, c, discriminant, t1, t2, t;

        aiStart   = this.point;
        ballStart = this.level.ball.point;
        ballEnd   = this.level.ball.turn.action.targetPoint;

        // If the ball is already inside the circle, then we don't try
        // to intercept it this way.
        if (Point.distanceBetween(aiStart, ballStart) <= Const.aiPawnMoveRange) {
            return null;
        }

        // Reference:
        // http://stackoverflow.com/questions/1073336/circle-line-collision-detection

        // Vectors and circle radius
        d = Point.subtract(ballEnd, ballStart);
        f = Point.subtract(ballStart, aiStart);
        r = Const.aiPawnMoveRange;

        // Set up a quadratic equation (at^2 + bt + c == 0) to find
        // the interception points t along the line of the ball at
        // which the line crosses the circle denoting our move range.
        a = Point.dot(d, d);
        b = 2 * Point.dot(f, d);
        c = Point.dot(f, f) - r*r;

        // Solve for t using the quadratic formula.
        discriminant = b*b - 4*a*c;
        if (discriminant < 0) { // Equation has no real roots
            return null;
        }
        discriminant = Math.sqrt(discriminant);
        t1 = (-b + discriminant)/(2*a);
        t2 = (-b - discriminant)/(2*a);
        if ((t1 < 0) || (t1 > 1)) {
            t1 = null;
        }
        if ((t2 < 0) || (t2 > 1)) {
            t2 = null;
        }

        if (t1 && t2) {
            // If there are two valid values of the parameter t, use
            // the bigger value so that the ball will be intercepted
            // later rather than sooner. This should look better since
            // the ball when passed moves faster than the AI pawn can
            // run and currently we stop the ball at the interception
            // point once it gets there. So, intercept later == less
            // time for the ball to lie still waiting for the AI to
            // catch it.
            t = Math.max(t1, t2);
        } else if (t1) {
            t = t1;
        } else if (t2) {
            t = t2;
        } else {
            return null;
        }

        return new Point(ballStart.x + t*d.x,
                         ballStart.y + t*d.y);
    }

    return AIPawn;

});
