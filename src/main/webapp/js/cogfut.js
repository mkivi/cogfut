define(function (require) {

    var Assets = require('assets');
    var Banner = require('banner');
    var Const  = require('const');
    var Level  = require('level');
    var Level0 = require('level0');
    var Level1 = require('level1');
    var Level2 = require('level2');
    var Level3 = require('level3');

    /**
     * @name CogFut
     *
     * @class Manages the high-level flow of the user's gaming
     * session. This currently includes transitions between playing
     * levels, replaying levels, and displaying the banner. Modal
     * overlay screens for profile management, level selection and
     * such are considered part of the GUI and are not dealt with
     * here. The GUI code, on the other hand, does use this class to
     * launch and receive notifications of game scenarios.
     *
     * @property {Kinetic.Stage} stage The Kinetic stage we lend to Banner and Levels.
     * @property {Level|Banner} level The level or banner currently active.
     * @property {function|null} onLevelFinishedFunc A function to call when a game has ended.
     */
    function CogFut() {
        var container;
        container = document.getElementById('cogfut-container');
        this.stage = new Kinetic.Stage({
            container : container,
            width     : container.offsetWidth + Const.windowSizeFudgePixels,
            height    : container.offsetHeight + Const.windowSizeFudgePixels
        });
        this.level = null;
        this.onLevelFinishedFunc = null;
        this.playLevel(-1);
    }

    /**
     * A complete, ordered list of levels available to play. Each
     * level is a subclass of the Level class. The public methods of
     * CogFut use index numbers to denote levels; a level's index
     * number corresponds to its zero-based index in this list.
     *
     * @const
     * @type Array
     */
    CogFut.levelClasses = [
        Level0,
        Level1,
        Level2,
        Level3
    ];

    /**
     * Return the zero-based index of the level currently being played
     * or replayed. Returns -1 if no level is being played. In that
     * case the banner is shown instead.
     *
     * @return {number}
     */
    CogFut.prototype.getLevelIndex = function () {
        var i;
        for (i=0; i<CogFut.levelClasses.length; i++) {
            if (this.level instanceof CogFut.levelClasses[i]) {
                return i;
            }
        }
        return -1;
    };

    /**
     * Play the given level from the beginning, disposing of any
     * previously active level first.
     *
     * @param {number} levelIndex The zero-based index of the desired level, or -1 for banner.
     * @throws {string} If levelIndex is out of bounds.
     */
    CogFut.prototype.playLevel = function (levelIndex) {
        if (this.level) {
            this.level.dispose();
        }
        this.level = null;
        if ((levelIndex < -1) || (levelIndex >= CogFut.levelClasses.length)) {
            throw 'CogFut: levelIndex out of bounds';
        }
        Assets.preloadThenCall(Const.images, function () {
            var levelClass;
            if (levelIndex === -1) {
                this.level = new Banner(this);
            } else {
                levelClass = CogFut.levelClasses[levelIndex];
                this.level = new levelClass(this);
            }
        }.bind(this));
    };

    /**
     * Play back a replay of the current game.
     *
     * @throws {string} If no level is active.
     */
    CogFut.prototype.playReplay = function () {
        if (!(this.level instanceof Level)) {
            throw 'CogFut: no level is active';
        }
        this.level.playReplay();
    };

    /**
     * Assign an event handler function to be called when a level has
     * been won or lost.  The function is called with one argument,
     * failMsg, which is a string containing a Finnish description of
     * the reason the game was lost. If the game was won then failMsg
     * is null.
     *
     * @param {Function|null} func The event handler function.
     */
    CogFut.prototype.onLevelFinished = function (func) {
        this.onLevelFinishedFunc = func;
    };

    return CogFut;

});
