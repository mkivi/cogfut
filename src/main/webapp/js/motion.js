define(function (require) {

    var Point = require('point');

    /**
     * @name Motion
     *
     * @class Represents a linear motion from one point to another at
     * a particular speed. Calculates the X and Y coordinates and the
     * angle of the motion at each point along the way.
     *
     * @property {number} xStep
     * @property {number} yStep
     * @property {number} maxSeconds
     * @property {number} angle
     * @property {Point} point
     * @property {Point} point0
     * @property {Point} point1
     * @property {number|null} startSeconds
     *
     * @constructor
     * @param {Point} point0
     * @param {Point} point1
     * @param {number} speed
     */
    function Motion(point0, point1, speed) {
        var dx, dy, len;
        dx = point1.x - point0.x;
        dy = point1.y - point0.y;
        len = Math.sqrt(dx*dx + dy*dy); // Pythagoras' theorem
        this.xStep = 0;
        this.yStep = 0;
        this.maxSeconds = 0;
        if ((len > 0) && (speed > 0)) {
            this.xStep = dx / len * speed;
            this.yStep = dy / len * speed;
            this.maxSeconds = len / speed;
        }
        this.angle = Math.atan2(dy, dx) + Math.PI/2;
        this.point = point0.clone();
        this.point0 = point0.clone();
        this.point1 = point1.clone();
        this.startSeconds = null;
    }

    Motion.prototype.updateForFrame = function(seconds) {
        this.startSeconds = this.startSeconds || seconds;
        seconds -= this.startSeconds;
        if ((seconds < 0) || (seconds > this.maxSeconds)) {
            return false;
        }
        this.point.x = this.point0.x + seconds * this.xStep;
        this.point.y = this.point0.y + seconds * this.yStep;
        this.angle = Point.angleBetween(this.point, this.point1) + Math.PI/2;
        return true;
    };

    return Motion;

});
