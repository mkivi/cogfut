define(function (require) {

    var AIPawn     = require('aipawn');
    var Const      = require('const');
    var Level      = require('level');
    var Pawn       = require('pawn');
    var PlayerPawn = require('playerpawn');
    var Point      = require('point');

    /**
     * @name Level0
     *
     * @class Implements level zero of the game: the sandbox. The
     * sandbox has no rules and no opponents; it is intended for
     * trying out the controls of the game.
     */
    function Level0(cogfut) {
        this.super = this.__proto__.__proto__;
        this.super.constructor.call(this, cogfut);
    };

    Level0.prototype.__proto__ = Level.prototype;

    /**
     * Make the gamepieces and rules for this level.
     */
    Level0.prototype.setupLevel = function () {
        new PlayerPawn(this, new Point(-5, -15));
        new PlayerPawn(this, new Point(+5, -15));
    };

    return Level0;

});
