define(function (require) {

    var Kinetic    = require('kinetic');

    var Assets     = require('assets');
    var Const      = require('const');
    var InGameMenu = require('ingamemenu');
    var Motion     = require('motion');
    var Pawn       = require('pawn');
    var PlayerPawn = require('playerpawn');
    var Point      = require('point');

    /**
     * @name Ball
     *
     * @class Represents the soccer ball. Its visual representations,
     * the main ball, the ghost ball, and the pass lines; and actions
     * specifying what is done to the ball each turn. Most game
     * actions have something to do with the ball, so this class also
     * serves as the central data structure in the game.
     *
     * @property {object} turn A data structure describing the current turn. This data structure is also contained in turnList.
     * @property {Array} turnList An ordered list of data structures describing the state and actions of this object during each turn of the game.
     * @property {Level} level The level being played.
     * @property {Point} point The current location of this object on the level.
     * @property {Motion|null} motion The current motion of this object if it is moving during an animation.
     * @property {Point} interceptedAt
     * @property {Point} wallPassPivot
     * @property {PlayerPawn} wallPawnCandidate
     * @property {Kinetic.Image} mainShape
     * @property {Kinetic.Image} ghostShape
     * @property {Kinetic.Circle} ghostRange
     * @property {Kinetic.Line} passLine A visual representation of the path of the next pass, if any.
     *
     * @constructor
     * @param {Level} level The level object that commissions us. We can access the Kinetic stage and all other game objects through this.
     * @param {Pawn|Point} initialSource
     */
    function Ball(level, initialSource) {
        this.turn = {
            frozen: false,         // If true, this turn can no longer be changed (has already been played).
            source: initialSource, // A PlayerPawn if that pawn possesses the ball at beginning of turn, or a Point where the ball lies freely.
            action: {type: null},  // Type is one of null or 'shot' or 'pass' or 'wallpass', other fields depend on type.
            shield: false          // Whether possessing pawn shields the ball at beginning (and through entire turn if dribbling).
        };
        this.turnList = [this.turn];
        this.level = level;
        this.point = null;
        this.motion = null;
        this.interceptedAt = null;
        this.wallPassPivot = null;
        this.wallPawnCandidate = null;
        this.mainShape = new Kinetic.Image(Assets.getImageAttrs(Const.images.ballMain));
        this.ghostShape = new Kinetic.Image(Assets.getImageAttrs(Const.images.ballGhost));
        this.ghostShape.setDraggable(true);
        this.ghostShape.setDragBoundFunc(this.ghostShapeDragBoundFunc.bind(this));
        this.ghostShape.on('dragend', this.ghostShapeDragEnd.bind(this));
        this.ghostShape.on('mouseover', this.ghostShapeMouseOver.bind(this));
        this.ghostShape.on('mouseout', this.ghostShapeMouseOut.bind(this));
        this.ghostRange = new Kinetic.Circle(Const.ballGhostRangeStyle);
        this.ghostRange.setVisible(false);
        this.ghostRange.setRadius(Point.pixelsFromUnits(Const.ballMoveRange));
        this.ghostRange.on('mouseout', this.ghostRangeMouseOut.bind(this));
        this.passLine = new Kinetic.Line(Const.passLineStyle);
        this.level.mainLayer.add(this.mainShape);
        this.level.mainLayer.add(this.ghostShape);
        this.level.mainLayer.add(this.ghostRange);
        this.level.mainLayer.add(this.passLine);
    }

    /**
     * This method is called at the beginning of the "setup phase" of
     * each turn. The setup phase is the phase where you can move the
     * pawns and the ball, etc. The setup phase ends when the "Play"
     * button is clicked.
     *
     * @param {number} turnIndex The zero-based index of the turn that is beginning
     */
    Ball.prototype.beginTurnSetup = function (turnIndex) {
        this.turn = this.turnList[turnIndex];
        this.setPointFromSource();
        switch (this.turn.action.type) {
        case 'shot':
        case 'pass':
            this.turn.action.targetPoint.assignToShape(this.ghostShape);
            break;
        case null:
            break;
        }
        this.mainShape.moveToBottom();
        this.mainShape.getLayer().draw();
        this.ghostShape.setVisible(!this.turn.frozen && (this.turn.source instanceof Pawn));
        this.ghostShape.getLayer().draw();
        this.interceptedAt = null;
    };

    /**
     * Set action of the current turn to null. It means that the
     * ball is either loose or the pawn is dribling.
     */
    Ball.prototype.setActionToNull = function () {
        this.turn.action = {type: null};
    };

    /**
     * Sets action of the current turn to shot.
     * 
     * @param {Point} targetPoint Point where the ball was shot.
     */
    Ball.prototype.setActionToShot = function (targetPoint) {
        this.turn.action = {type: 'shot', targetPoint: targetPoint};
    };

    /**
     * Sets action of the current turn to pass.
     * 
     * @param {Point} targetPoint Point where the ball was passed.
     */
    Ball.prototype.setActionToPass = function (targetPoint) {
        this.turn.action = {type: 'pass', targetPoint: targetPoint};
    };

    /**
     * Sets action of the current turn to wall pass.
     * 
     * @param {PlayerPawn} wallPawn Pawn who passes the ball back to 
     * the original pawn.
     */
    Ball.prototype.setActionToWallPass = function (wallPawn) {
        this.turn.action = {type: 'wallpass', wallPawn: wallPawn};
    };

    /**
     * This function is called everytime ghostShape moves. Blocks
     * moving when going over moverange. Updates passing line.
     *
     * @param {object} pos Ghost shape's point in pixels
     * @return {object} Ghost shape's point in pixels after we have made sure 
     * it's inside the move range
     */
    Ball.prototype.ghostShapeDragBoundFunc = function (pos) {
        var x, y, dx, dy, len, scale;
        x = Point.pixelsFromUnits(this.point.x);
        y = Point.pixelsFromUnits(this.point.y);
        dx = pos.x - x;
        dy = pos.y - y;
        len = Math.sqrt(dx*dx + dy*dy); // Pythagoras' theorem
        scale = Point.pixelsFromUnits(Const.ballMoveRange) / len;
        if(scale < 1) {
            pos = {
                x: x + dx*scale,
                y: y + dy*scale
            };
        }
        this.passLine.setPoints([this.mainShape.getX(),this.mainShape.getY(),
                                 this.ghostShape.getX(),this.ghostShape.getY()]);
        this.passLine.setVisible(true);
        return pos;
    };

    /**
     * This method is called when dragging of the ghost shape ends. 
     * Tries to find a pawn which could be a wall pawn.
     *
     * @private
     * @param  {Point} pt Point where the ghost ball is
     * @return {PlayerPawn|Null} PlayerPawn if it finds a candidate
     * otherwise null
     */
    Ball.prototype.wallPawnCandidateAt = function (pt) {
        return this.level.eachGameObject(function (pawn) {
            if ((pawn instanceof PlayerPawn) && (this.turn.source !== pawn)) {
                if (Point.distanceBetween(pt, pawn.turn.targetPoint) <= Const.maxPlayerDispossessionDistance) {
                    return pawn;
                }
            }
        }.bind(this));
    };

    /**
     * When dragging of the ghost shape ends, this method opens the popup menu or 
     * calls the pass method. Depends on if we got a pawn close which could be a wall pawn.
     */
    Ball.prototype.ghostShapeDragEnd = function () {
        this.wallPawnCandidate = this.wallPawnCandidateAt(Point.fromShape(this.ghostShape));
        this.pass();
        if (this.wallPawnCandidate) {
            this.level.showPopupMenu(this.buildPassMenu(), this.ghostShape);
        }
    };

    /**
     * This method makes a pass and calls pawn's passTheBall method. If we got the ghost shape
     * on the main shape, we revert the pass.
     *
     * @private
     */
    Ball.prototype.pass = function() {
        this.turn.source.passTheBall();
        if (kineticRectContainsShape(this.level.goalRect, this.ghostShape)) {
            this.setActionToShot(Point.fromShape(this.ghostShape));
        } else if (this.calcGhostBallDistanceFromBall() > Const.ballSnapDistance) {
            this.setActionToPass(Point.fromShape(this.ghostShape));
        } else {
            this.setActionToNull();
            this.setPoint(this.point);
            this.turn.source.revertPass();
        }
        this.ghostShape.moveToTop();
        this.level.mainLayer.draw();
    };

    /**
     * Shows move range when hovering mouse over the ghost shape
     */
    Ball.prototype.ghostShapeMouseOver = function () {
        this.ghostRange.show();
        this.ghostRange.moveToBottom();
        this.level.mainLayer.draw();
    };

    /**
     * Hides move range when hovering mouse out of the ghost shape
     */
    Ball.prototype.ghostShapeMouseOut = function () {
        this.ghostRange.hide();
        this.level.mainLayer.draw();
    };
    
    /**
     * Hides move range when hovering mouse out of the ghost range
     */
    Ball.prototype.ghostRangeMouseOut = function () {
        this.ghostRange.hide();
        this.level.mainLayer.draw();
    };

    /**
     * Builds menu for the pass
     *
     * @private
     * @return {InGameMenu} Menu with pass and wallpass options
     */
    Ball.prototype.buildPassMenu = function () {
        var menu;
        menu = new InGameMenu();
        menu.add('Syöttö',
                 true,
                 this.pass.bind(this));
        menu.add('Seinäsyöttö kautta',
                 true,
                 this.wallPassCommand.bind(this));
        return menu;
    };

    /**
     * Makes a wallpass and calls pawn's passTheBall method
     *
     * @private
     */
    Ball.prototype.wallPassCommand = function () {
        this.turn.source.passTheBall();
        this.setActionToWallPass(this.wallPawnCandidate);
        this.wallPawnCandidate = null;
        this.updateVisualsIfWallPass();
    };

    /**
     * Draws and moves the ghost shape back to the Pawn who made the pass and 
     * draws or updates passing lines
     */
    Ball.prototype.updateVisualsIfWallPass = function() {
        var p0, p1, p2;
        if (this.level.ball.turn.action.type !== 'wallpass') {
            return;
        }
        p0 = this.point;
        p1 = this.turn.action.wallPawn.turn.targetPoint;
        p2 = this.turn.source.calcBallTargetPoint();
        p2.assignToShape(this.ghostShape);
        this.level.ball.passLine.setPoints(Point.xyPixelArrayFromPoints(p0, p1, p2));
        this.passLine.setVisible(true);
        this.passLine.getLayer().draw();
        this.ghostShape.getLayer().draw();
    };

    /**
     * This method is called at the end of the "setup phase" of each
     * turn, just before the animation is started.
     */
    Ball.prototype.endTurnSetup = function () {
        this.passLine.setVisible(false);
        this.ghostShape.hide();
        this.ghostShape.getLayer().draw();
        this.motion = null;
        switch (this.turn.action.type) {
        case 'shot':
            this.motion = new Motion(this.point, this.turn.action.targetPoint, Const.ballShotSpeed);
            break;
        case 'pass':
            if (this.interceptedAt) {
                this.motion = new Motion(this.point, this.interceptedAt, Const.ballPassSpeed);
            } else {
                this.motion = new Motion(this.point, this.turn.action.targetPoint, Const.ballPassSpeed);
            }
            break;
        case 'wallpass':
            this.wallPassPivot = this.turn.action.wallPawn.calcBallTargetPoint();
            this.motion = new Motion(this.point, this.wallPassPivot, Const.ballPassSpeed);
            break;
        case null:
            break;
        }
    };

    /**
     * This method is called once for each animation frame while a
     * turn is being animated. It computes the position of the object
     * during that frame and moves the object to that position on the
     * screen as well. It returns true if the object may still move,
     * or false if the object has reached the end of its motion for
     * this turn.
     *
     * @param {number} Kinetic frame's starting seconds
     * @return {boolean} Are we still moving
     */
    Ball.prototype.updateForFrame = function (seconds) {
        switch (this.turn.action.type) {
        case 'shot':
        case 'pass':
            if (this.motion.updateForFrame(seconds)) {
                this.setPoint(this.motion.point);
                return true;
            }
            break;
        case 'wallpass':
            if (this.motion.updateForFrame(seconds)) {
                this.setPoint(this.motion.point);
                return true;
            }
            if (this.wallPassPivot) {
                this.motion = new Motion(this.wallPassPivot, this.turn.source.calcBallTargetPoint(), Const.ballPassSpeed);
                this.setPoint(this.motion.point);
                this.wallPassPivot = null;
                return true;
            }
            break;
        case null:
            this.setPointFromSource();
            break;
        }
        return false;
    };

    /**
     * Add a new turn at the end of the object's turn list and freeze
     * the prior turn. The new turn is derived from the prior one.
     */
    Ball.prototype.appendTurn = function () {
        var nextSource, shield;
        shield = this.turn.shield;
        switch (this.turn.action.type) {
        case 'shot':
        case 'pass':
            nextSource = this.point.clone();
            shield = false;
            break;
        case 'wallpass':
            nextSource = this.turn.source;
            shield = false;
            break;
        case null:
            nextSource = this.turn.source;
            break;
        }
        if (this.turnList.length > 0) {
            this.turnList[this.turnList.length-1].frozen = true;
        }
        this.turnList.push({
            frozen: false,
            source: nextSource,
            action: {type: null},
            shield: shield
        });
    };

    /**
     * Setting a point for the ball from the source.
     *
     * @private
     */
    Ball.prototype.setPointFromSource = function () {
        if (this.turn.source instanceof Point) {
            this.setPoint(this.turn.source);
        } else if (this.turn.shield) {
            this.setPoint(this.turn.source.calcShieldingPoint());
        } else {
            this.setPoint(this.turn.source.calcPointInFront(this.mainShape));
        }
    };

    /**
     * Setting a point for the ball
     * 
     * @param {Point} newPoint new point for the ball
     */
    Ball.prototype.setPoint = function (newPoint) {
        this.point = newPoint.clone();
        this.point.assignToShape(this.mainShape);
        this.point.assignToShape(this.ghostRange);
        this.point.assignToShape(this.ghostShape);
    };

    /**
     * Calculates distance between ghost ball and ball
     *
     * @private
     * @return {number} Distance in points
     */
    Ball.prototype.calcGhostBallDistanceFromBall = function () {
        return Point.distanceBetween(Point.fromShape(this.mainShape),
                                     Point.fromShape(this.ghostShape));
    };

    /**
     * Checks if the shape is inside the rectangle
     * 
     * @param  {Kinetic.Rect} rect  
     * @param  {Kinetic.Image} shape 
     * @return {Boolean} True if the shape is inside the rectangle
     */
    function kineticRectContainsShape(rect, shape) {
        var x, y, x0, y0, x1, y1;
        x = shape.getX();
        y = shape.getY();
        x0 = rect.getX();
        y0 = rect.getY();
        x1 = x0 + rect.getWidth();
        y1 = y0 + rect.getHeight();
        return ((x0 <= x) && (x <= x1) &&
                (y0 <= y) && (y <= y1));
    }

    return Ball;

});
