define(function (require) {

    var Kinetic = require('kinetic');

    /**
     * @name Assets
     *
     * @class Manages game assets, currently just images. Sound clips
     * and other stuff could be added later. The tricky thing with
     * assets is that they reside in external files that are not
     * loaded with the scripts so we must track them to ensure they're
     * all ready before we try to use them. It can also be tricky to
     * compure the parameters needed to use them in the game, so this
     * class takes care of that too.
     */
    var Assets = {};

    /**
     * A dictionary mapping relative filenames to Image objects.
     */
    Assets.assets = {};

    /**
     * A global prefix for all asset file names. Takes effect for all
     * assets added after changing it.
     */
    Assets.basePath = "";

    /**
     * Load all of the given assets, wait until they are all loaded,
     * then call the given function.
     *
     * @param {object} dict A dictionary whose keys are ignored and
     * whose values are the filenames of the assets to load.
     *
     * @param {function} func The function to call once all of the
     * assets have been loaded.
     */    
    Assets.preloadThenCall = function (dict, func) {
        var key, filename, image, pending, gotAny, imageOnloadFunc;
        pending = 0;
        imageOnloadFunc = function() {
            Assets.assets[this.assetFilename] = this;
            if (--pending <= 0) {
                func();
            }
        };
        gotAny = false;
        for (key in dict) {
            if (dict.hasOwnProperty(key)) {
                filename = dict[key];
                if (!Assets.assets.hasOwnProperty(filename)) {
                    pending++;
                    gotAny = true;
                    image = new Image();
                    image.onload = imageOnloadFunc;
                    image.assetType = 'image';
                    image.assetFilename = filename;
                    image.src = Assets.basePath+filename; // Browser may resolve this from relative to absolute.
                }
            }
        }
        if (!gotAny) {
            func();
        }
    };


    /**
     * Retrieve an image asset as a HTML Image object.
     *
     * @param {string} filename The filename of the asset, in the exact form given to Assets to load it.
     * @return {Image} The Image object.
     */
    Assets.getImage = function (filename) {
        var asset;
        asset = Assets.assets[filename];
        if (!asset || (asset.assetType !== 'image')) {
            throw "Assets: image asset expected: "+filename;
        }
        return asset;
    };

    /**
     * Retrieve an image asset as a Kinetic attributes/config object.
     *
     * @param {string} filename The filename of the asset, in the exact form given to Assets to load it.
     * @return {object} The Kinetic attributes/config object.
     *
     * @example
     * this.mainShape = new Kinetic.Image(Assets.getImageAttrs(Const.images.ballMain));
     */
    Assets.getImageAttrs = function (filename) {
        var image;
        image = Assets.getImage(filename);
        return {
            image  : image,
            width  : image.width,
            height : image.height,
            offset : [image.width/2, image.height/2]
        };
    };

    return Assets;

});
