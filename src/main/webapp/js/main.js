/*
* File:        main.js
* Description: Lanches Cogfut application when window is loaded and controls UI functions
* Author:      Mika Kivi etc.
* Language:    Javascript
* License:     GPL v2
* Project:     CogFut
* 
*
* This source file is free software, under either the GPL v2 license
* available at:
*   http://datatables.net/license_gpl2
*
*/

require.config({
    baseUrl: './js/',
    paths: {
        bootstrap: 'libs/bootstrap/js/bootstrap',
        jquery:    'libs/jquery/jquery-1.8.2',
        kinetic:   'libs/kinetic-js/kinetic-v4.7.2'
    },
    shim: {
        bootstrap: {
            deps: ['jquery']
        },
        kinetic: {
            deps: [],
            exports: 'Kinetic'
        }
    }
});

/*
TÄN VOI VARMAAN POISTAA???
Function.prototype.extend = function (subclass) {
    subclass.prototype = new this();
    subclass.prototype.__super = subclass.prototype.constructor;
    subclass.prototype.constructor = subclass;
    return subclass;
};*/


require(['cogfut', 'api', 'jquery', 'bootstrap'], function (CogFut, Api, $) {
/**
*
* @name Main
* 
* @class Class that handle UI and game. Reacts users clicks and make change to UI. 
*  Handles modals that need to be shown when user clikcs some part of UI.
*  Updates page when it is needed. Handels buttons outside the game.
*
* @property {Cogfut} cogfut Game engine that controls game to be played at the time. Control given to window so it is easy to use
* @property {string} login1ModalTab Told wich one were clicked Rekisteroidy or Kirjaudu this is going to be removed 
* @property {boolean} infoDivShown If true infoDiv is shown at the time. False invoDiv is hidden/removed
* @property {boolean} levelFinished True: level is finished. Then starts replay when video modal is hidden. False: level not finished. When video modal is closed just closes it :).
*
*/
    $(document).ready(function () {

        window.cogfut = new CogFut();
        var loginModalTab = null;
        var infoDivShown = false;
        var levelFinsihed = false;
        /**
        * If player is allready loged in this will hide login and register buttons.
        * If not loged in will be hidden logout button
        */
        if(localStorage.getItem('username')){
            $("#logout-btn").html("<div class='login-name'>" + localStorage.getItem('username') + "</div>" + "<div class='login-name-text'> Kirjaudu ulos</div>");
            $("#logout-btn").show();
            $("#Kirjaudu-btn").hide();
            $("#Rekisteroidy-btn").hide();
        }else{
            $("#logout-btn").hide();
            $("#Kirjaudu-btn").show();
            $("#Rekisteroidy-btn").show();
        }
 

        /**
        * Function that is called when Rekisteroidy-btn is pushed down
        */
        $('#Rekisteroidy-btn').on('mousedown', function (event) {
            openLoginAndRegister(2);
        });

        /**
        * Function that is called when Kirjaudu-btn is pushed down
        */
        $('#Kirjaudu-btn').on('mousedown', function (event) {
            openLoginAndRegister(1);
        });

        /**
        * Function that is called when sandbox-info-input is pushed down 
        */
        $('#sandbox-info-input').on('mousedown', function (event) {
            openInfoModal(0);
        });

        /**
        * Function that is called when behind-info-input is pushed down
        */
        $('#behind-info-input').on('mousedown', function (event){
            openInfoModal(1);
        });

        /**
        * Function that is called when wallPass-info-input is pushed down
        */
        $('#wallPass-info-input').on('mousedown', function (event){
            openInfoModal(2);
        });

        /**
        * Function that is called when diagonalPass-info-input is pushed down
        */
        $('#diagonalPass-info-input').on('mousedown', function (event){
            openInfoModal(3);
        });

        /**
        * Function that is called when info-tooltip-button is pushed down
        * If infoDiv is allreade shown will remove it 
        * Otherwise it will create new infoDiv adds it to web pages left side 
        */
        $('#info-tooltip-btn').on('mousedown', function (event){
            if(!infoDivShown){
                addInfoDiv();
            }
            else{
                removeInfoDiv();
            }
        });

        /**
        * Function that is called when video-modal-replay-btn is pushed down
        */
        $('#video-modal-replay-btn').on('mousedown', function (event) {
            playReplay();
        });

        /**
        * Function that is called when sandbox-play-un
        */
         $('#sandbox-play-input').on('mousedown', function (event) {
            playLevel(0);
        });

        /**
        * Function that is called when behind-play-input is pushed down
        */
        $('#behind-play-input').on('mousedown', function (event) {
            playLevel(1);
        });

        /**
        * Function that is called when wallPass-play-input is pushed down
        */
        $('#wallPass-play-input').on('mousedown', function (event) {
            playLevel(2);
        });

        /**
        * Function that is called when diagonalPass-play-input is pushed down
        */
        $('#diagonalPass-play-input').on('mousedown', function (event) {
            playLevel(3);
        });

        /**
        * Function that is called when close-level-info-btn is pushed down
        */
        $('#close-level-info-btn').on('mousedown', function (event) {
            closeInfoModal(); 
        });

        /**
        * Function that is called when login-submit-btn is pushed down
        */
        $('#login-submit-btn').on('mousedown', function (event){
            login();
        });

        /**
        * Function that is called when logout-btn is pushed down
        */
        $('#logout-btn').on('mousedown', function (event){
            logout();
        });

        /**
        * Function that is called when register-submit-btn is pushed down
        */
        $('#register-submit-btn').on('mousedown', function (event) {
            register();
        });

        /**
        * Function that is called when cogfut-logo is pushed down
        * Launches start page with logo
        */
        $('#cogfut-logo').on('mousedown', function (event){
            window.cogfut.playLevel(-1);
        });

        /**
        * Function that is called when profile-btn is pushed down
        */
        $('#profile-btn').on('mousedown', function (event) {
            getProfile();
        });


   
        /**
         * This function is called when loginModal is shown. 
         * First empty all warning from modal
         * Sets focus and header for modal
         */
        $('#loginModal').on('shown', function (event) {
            $("#wrong-username-password-warning").empty();
            $("#missing-register-information-warning").empty();
            $("#pass-not-match").empty();
            $("#" + loginModalTab + "-input").focus();
            document.getElementById("header").innerHTML = login1ModalTab;
        });

        /**
         * This function is called when loginModals tab is changed
         * First empty all warning from modal
         * Sets focus and header for modal
         */
        $('a[data-toggle="tab"]').on('shown', function (event) {
            $("#wrong-username-password-warning").empty();
            $("#missing-register-information-warning").empty();
            $("#pass-not-match").empty();
            loginModalTab = $(event.target).attr("href").replace("#", "");
            $("#" + loginModalTab + "-input").focus();
        });

        /**
         * This function is called when contactModal is opened
         * Sets focus 
         */
        $('#contactModal').on('shown', function (event){
            $("#contact-message" ).focus();
        });

        /**
        * This funvtion is called when videoModal is hide
        * First removes video from modal
        * Check if modal was opened because end of level
        * If true, will launch replay will start
        */
        $('#videoModal').on('hide', function (event){
            emptyVideoBlock();
            if(levelFinsihed){
                levelFinsihed = false;
                playReplay();
            }
        });

        /**
        * This function is called when levelInfoModal is hide
        */
        $('#levelInfoModal').on('hide', function (event){
            emptyInfoBlock();
        });

        /**
        * Function that gets profile infomation from server  
        * Makes calls using Api class
        * If call returns error will use mock up data
        * If false has been returned will add info text
        * Otherwise will use data that has been returned to make profile modal
        */
        function getProfile () {
            Api.profile(function (data) {
                if (data === undefined) {
                    // Problem with server, or no server connection available.
                    // Just use some mock data.
                    addProfileInfo({"username":"LittiPeukku","games":0,"won":0,"lost":0});
                } else if (!data) {
                    notLogedIn();
                } else {
                    addProfileInfo(data);
                }
            });
        }

        /**
        * Function that checks login information and logs user in.
        * Gets username and password values
        * Both have to be entered else will add error message to modal
        * Use Api class to make call to server
        * Call returns false:   Adds warning to modal
        * Call returns true:    Will fix UI looklike loged in also saves username to localstore
        *                       Hides loginModal
        */
        function login () {
            var username = $('#Kirjaudu-input').val();
            var password = $('#inputPassword').val();
            if(username === "" || password ===""){
                wrongUserOrPass();
                $("#Kirjaudu-input").focus();
                return;
            }
            //THIS CODE IS FOR TESTING WITHOUT SERVER
            /*localStorage.setItem("username", username);
            $("#logout-btn").html("<div class='login-name'>" + username + "</div>" + "<div class='login-name-text'> Kirjaudu ulos</div>");
            $("#Kirjaudu-btn").hide();
            $("#Rekisteroidy-btn").hide(); 
            $("#logout-btn").show();
            $('#loginModal').modal('hide');*/     
            Api.login(username, password, function (data) {
                if (data === undefined) {
                    addWarning("wrong-username-password-warning", "Ongelma palvelimen kanssa. Yritä myöhemmin uudelleen");
                } else if (!data) {
                    wrongUserOrPass();
                } else {
                    localStorage.setItem("username", username);
                    $("#logout-btn").html("<div class='login-name'>" + username + "</div>" + "<div class='login-name-text'> Kirjaudu ulos</div>");
                    $("#Kirjaudu-btn").hide();
                    $("#Rekisteroidy-btn").hide();
                    $("#logout-btn").show();
                    $('#loginModal').modal('hide');
                }
            });
        }

        /**
        * Log out function.
        * Use Api class to close connection to server
        * At the time function do not care about return from server side
        * Changes UI look like loged out
        * Removes username from localstorage
        * Launches banner level with logo
        */
        function logout () {
             Api.logout(function (data) {
                if (data === undefined) {
                    // Problem with server, or no server connection available.
                    //logOutFailed();
                } else if (!data) {
                    //logOutFailed();
                } else {
                    // Nothing to do
                }
                // Currently, no matter what happens, pretend the logout was successful.
                localStorage.removeItem("username");
                $("#Kirjaudu-btn").show();
                $("#Rekisteroidy-btn").show();
                $("#inputPassword").val("");
                $("#logout-btn").hide();
                window.cogfut.playLevel(-1);
            });
        }

        /**
        * Function that will register user
        * Gets username, age, and password from input
        * Checks that all required information is given
        * Checks that both passwords were same
        * Use Api class make server call
        * Call return false:    adds error to modal
        * Call return true:     makes UI look like loged in 
        *                       stores user name to localstorage
        *                       hides loginModal
        */
        function register () {
            var username = $("#Rekisteroidy-input").val();
            var age = $("#age-input").val();
            var password = $("#password-input").val();
            var rePassword = $("#repassword-input").val();
            if (username === "" || age === "" || password === "") {
                addWarning("missing-register-information-warning", "Joitain tietoja puuttui vielä");
                $("#Rekisteroidy-input").focus();
                return;
            }
            if(password !== rePassword){
                addWarning("pass-not-match", "Salasanat eivät olleet samat");
                $("#password-input").val("");
                $("#repassword-input").val("");
                $("#password-input").focus();
                return;
            }
            //THESE ARE FOR TESTING WITHOUT SERVER
            /*localStorage.setItem("username", username);
            $("#logout-btn").html("<div class='login-name'>" + username + "</div>" + "<div class='login-name-text'> Kirjaudu ulos</div>");
            $("#Kirjaudu-btn").hide();
            $("#Rekisteroidy-btn").hide();
            $("#logout-btn").show();
            $('#loginModal').modal('hide');
            return;*/ //TEST CODE ENDS HERE
            
            Api.register(username, password, age, function (data) {
                if (data === undefined) {
                    addWarning("missing-register-information-warning", "Ongelma palvelimen kanssa. Yritä myöhemmin uudelleen");
                } else if (!data) {
                    registerFailed();
                } else {
                    localStorage.setItem("username", username);
                    $("#logout-btn").html("<div class='login-name'>" + username + "</div>" + "<div class='login-name-text'> Kirjaudu ulos</div>");
                    $("#Kirjaudu-btn").hide();
                    $("#Rekisteroidy-btn").hide();
                    $("#logout-btn").show();
                    $('#loginModal').modal('hide');
                }
            });
        }

        /**
        * Function that makes loginModal look right
        * Use tabNum to choose which tab is active
        * Emptyes all input values
        * Removes all warnings from modal
        * Sets modal tab and input active
        * 
        * @param {number} tabNum is tab number which one is shown 1 = Kirjaudu tab, 2 = Rekisteroidy tab 
        */
        function openLoginAndRegister (tabNum) {
            if(tabNum === 1){
                loginModalTab = 'Kirjaudu';
            }
            else {
                loginModalTab = 'Rekisteroidy';
            }
            //Will empty input areas
            $("#inputPassword").val("");
            $("#Rekisteroidy-input").val("");
            $("#Kirjaudu-input").val("");
            $("#age-input").val("");
            $("#inputPassword").val("");
            $("#password-input").val("");
            $("#repassword-input").val("");

            //Sets right tab to be shown
            $(".tab-pane, #main-tabs li").removeClass("active");
            $("#wrong-username-password-warning").empty();
            $("#pass-not-match").empty();
            $("#missing-register-information-warning").empty();
            $("#" + loginModalTab).addClass("active");
            $("#main-tabs li.li-" + loginModalTab).addClass("active");
        }


        /**
        * Function that will create error message  when logout is failed
        * Shows warning modal with message 
        */
        function logOutFailed(){
            addWarning("warning-modal-warning", "Ulos kirjaus epäonnistui. Yritä myöhemmin uudelleen");
            $("#warning-Modal").modal('show');
        }

        /**
        * Function that will add error message to profileModal
        * Error is added when you are not loged in 
        */
        function notLogedIn() {
            addWarning("profile-modal-body", "Et ole kirjautunut sisään");
            $('#profile-modal').modal('show');
        }

        /**
        * Function that will called to make warning 
        * When username or password is wrong  
        */
        function wrongUserOrPass(){
            addWarning("wrong-username-password-warning", "Tunnus/salasana väärin");
        }

        /**
        * Function that will be called to make warning when username is allready in use
        */
        function registerFailed(){
            addWarning('missing-register-information-warning', 'Tunnus on jo käytössä');
        }

        /**
        * Function that will remove info-Div from page
        * Sets infoDivShown false
        */
        function removeInfoDiv () {
            $('#info-Div').remove();
            infoDivShown = false;
        }

        /**
        * Function that will remove video from videoModal
        * Used when video modal is closed
        */
        function emptyVideoBlock () {
            $('#video-modal-body').empty();
        }

        /**
        * Function that will remove video and text from infoModal
        * Used when infoModal is closed 
        */
        function emptyInfoBlock () {
            $('#level-info-text-div').empty();
        }

        /**
        * Function will add infoDiv to left side of page
        * Gets level that is played at the time
        * Gets level info text
        * First add div with infotext
        * Then adds info picture if it has been created
        * Adds footer to div
        * Adds close button wich will remove info-Div
        * If user is playing some level will add watch video input
        * Then adds function listeners for those buttons
        * Sets infoDivShown true
        */
        function addInfoDiv () {

            var level = window.cogfut.getLevelIndex();

            var text = chooseInfoText(level);

            var startPos = $(document).height() /2 - 325;

            var divTag = document.createElement('div');
            divTag.id = "info-Div";
            divTag.className = "alert alert-info";
            divTag.style.left = "15%";
            divTag.style.position = "absolute";
            divTag.style.width = "300px";
            divTag.style.top = startPos + "px";
            divTag.style.border = "5px solid";
            divTag.innerHTML = text;
            $('#level-info-container').append(divTag);


            if(level > 0 && level < 4){
                var infoPic = document.createElement('img');
                infoPic.src = 'img/level' + level + 'info.png';
                infoPic.className = "img-rounded";
                infoPic.style.width = "300px";
                infoPic.style.height = "300px";
                $('#info-Div').append(infoPic);
            } 



            var footerDiv = document.createElement('div');
            footerDiv.id = "info-footer-div";
            footerDiv.className = ("modal-footer");
            $('#info-Div').append(footerDiv);

            var hideInfoInput = document.createElement('input');
            hideInfoInput.type = "button";
            hideInfoInput.className = "btn";
            hideInfoInput.id = "hide-Info-Input";
            hideInfoInput.value = "Piilota info";
            $('#info-footer-div').append(hideInfoInput);

            if(level > 0){
                var watchVideoInput = document.createElement('input');
                watchVideoInput.type = "button";
                watchVideoInput.className = "btn btn-info";
                watchVideoInput.id = "watch-video";
                watchVideoInput.value = "Katso video";
                $('#info-footer-div').append(watchVideoInput);
            }

            $('#watch-video').on('mousedown', function (event){
                openVideoModal(level);
            });

            $('#hide-Info-Input').on('mousedown', function (event) {
                removeInfoDiv();
            });

            infoDivShown = true;
        }

        /**
        * Function adds profile information to profile modal
        * First emptyes modal header and body
        * Adds username from data to header
        * Creates to divs for body to set numbers and information to same line
        * Adds information from data to divs
        * 
        * @param {JSON} data sample of JSON looks like this {"username":"LittiPeukku","games":0,"won":0,"lost":0}
        */
        function addProfileInfo(data) {
            $('#profile-modal-body').empty();
            $('#profile-modal-header').empty();
            var headerDiv = document.createElement('div');
            headerDiv.innerHTML = "<h1>" + data.username + "</h1>";
            $('#profile-modal-header').append(headerDiv);

            var infomationDiv = document.createElement('div');
            infomationDiv.className = "information-div pull-right";
            infomationDiv.innerHTML = "<h3 class='text-info'>" + data.games + "</h3></br><h3 class='text-success'>" + data.won + "</h3></br><h3 class='text-error'>" + data.lost + "</h3>";
            $('#profile-modal-body').append(infomationDiv);


            var divTag = document.createElement('div');
            divTag.id = "profile-info-div";
            divTag.className = "profile-info-div"
            divTag.width = "50%";
            divTag.innerHTML = "<h3>Games:</h3></br><h3>Won:</h3></br><h3>Lost:</h3>";
            $('#profile-modal-body').append(divTag);
            $('#profile-modal').modal('show');
        }
        
        /**
         * Function will open levelInfoModal and will add video and info text to body
         * Gets levelinfo text and level headertext
         * Emptyes div from old data
         * Adds info text and video to body
         * Video will start automatically
         * Adds header to modal header
         * Reset info-play-button function when clicked
         * Shows levelInfoModal
         *
         * @param {number} levelNum is number of level that information shuold be added
         */
        function openInfoModal (levelNum) {
            $('#chooseLevelModal').modal('hide');
            var text = chooseInfoText(levelNum);
            var headerText = chooseHeaderText(levelNum);

            $('#level-info-text-div').empty();
            var divTag = document.createElement('div');
            divTag.id = "info-div";
            divTag.className = "text-info";
            divTag.innerHTML = text;
            $('#level-info-text-div').append(divTag);

            var video = $('<video width="300" height="264" autoplay></video>')
            .append('<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + levelNum +'video.mp4" type="video/mp4"></source>')
            .append('<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + levelNum +'video.webm" type="video/webm"></source>')
            .append('<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + levelNum +'video.ogv" type="video/ogg"></source>')
            .appendTo($("#info-div"));

            $('#level-info-header').empty();
            var headerTag = document.createElement('h1');
            headerTag.innerHTML = headerText + " INFO";
            $('#level-info-header').append(headerTag);

            $('#info-play-button').on('mousedown', function (event) {
                playLevel(levelNum);
            });

            $("#levelInfoModal").modal('show');
        }

        /**
        * Function that will close infoModal and shows chooseLevelModal
        */
        function closeInfoModal () {
            $('#levelInfoModal').modal('hide');
            $('#chooseLevelModal').modal('show');
        }
       
        /**
        * Functoin that will open videoModal and adds video to it
        * Gets header text
        * Shows close button and hides watch replay button
        * Emptyes modal header and adds header text to it
        * Adds target levels video to body
        *
        * @param {number} levelNumber is number of level that is played at the time
        */
        function openVideoModal (levelNumber) {
            var headerText = chooseHeaderText(levelNumber);
            $('#video-modal-close-btn').show();
            $('#video-modal-replay-btn').hide();
            $('#video-modal-header').empty();
            var headerTag = document.createElement('h1');
            headerTag.innerHTML = headerText;
            $('#video-modal-header').append(headerTag);
            $("#video-modal-body").html(
                '<video width="500" height="264" autoplay>' +
                    '<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + levelNumber +'video.mp4" type="video/mp4"></source>' +
                    '<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + levelNumber +'video.webm" type="video/webm"></source>' +
                    '<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + levelNumber +'video.ogv" type="video/ogg"></source>' +
                '</video>');
            $('#videoModal').modal('show');
        }

        /*
         * Function that adds warning to target div
         * @param {string} divName = Target div
         * @param {string} warningText = Text to be added as warning
         */
        function addWarning(divName, warningText){
            $("#" + divName).empty();
            var divTag = document.createElement("div");
            divTag.id = "warning-Div";
            divTag.className = "alert alert-error";
            divTag.innerHTML = warningText;
            $("#" + divName).append(divTag);
        }


        /**
        * Function that starts replay
        * Removes infoDiv
        * Hides videoModal
        * Adds footer div to container
        * Adds replay button and choose new level button to footer
        * Sets up click event listeners fo buttons
        * Calls cogfut to start replay
        */
        function playReplay () {
            removeInfoDiv();
            $('#videoModal').modal('hide');
            $('#play-Replay-footer-div').empty();
            var divTag = document.createElement("div");
            divTag.id = "play-Replay-footer-div";
            divTag.className = "container-footer-div";
            $('#cogfut-container').append(divTag);
            var rePlayInput = document.createElement('input');
            rePlayInput.type = "button";
            rePlayInput.className = "btn btn-info pull-right";
            rePlayInput.id = "replay-input";
            rePlayInput.value = "Katso uusinta uudelleen";
            $('#play-Replay-footer-div').append(rePlayInput);
            var chooseNewLevelInput = document.createElement('input');
            chooseNewLevelInput.type = "button";
            chooseNewLevelInput.className = "btn btn-primary pull-right";
            chooseNewLevelInput.id = "choose-new-level-input";
            chooseNewLevelInput.value = "Valitse uusi kenttä";
            $('#play-Replay-footer-div').append(chooseNewLevelInput);
            $('#replay-input').on('mousedown', function (event) {
                playReplay();
            });
            $('#choose-new-level-input').on('mousedown', function (event){
                $('#chooseLevelModal').modal('show');
            });
            window.cogfut.playReplay();
        }

        /**
        * Function that will return levels info text,
        * If there is no level text create will reprot 
        * 
        * @param {number} levelNum is number of level that is played atm
        * @return {string} returnText is infomation text
        */
        function chooseInfoText(levelNum){
            var returnText;
            switch (levelNum){
            case 0: returnText = "1) Hiekkalaatikko kentässä voit kokeilla kuinka pelaajien liikkuminen taphatuu. </br></br> 2) Samalla sinulla on mahdollisuus tutusttua pelin eri ominaisuuksiin.</br></br> 3) Ohjeistuksen avulla opit pelin ominaisuudet. (Ohjeistusta ei ole vielä tehty)." ;
                return returnText;
            case 1: returnText = "1) Hyökkäävien pelaajien 1 ja 2 tavoite on paikkaa (laitaa) vaihtaen saada pallo pelatuksi rangaistusalueen sisään ja laukoa pallo maaliin.  </br></br> 2) Luo tilaa pallottomalle pelaajalle pallottoman pelaajan takaakierrolla ja ajoittamalla pallollisen pelaajan syötön oikea-aikaisesti pallottomalle pelaajalle. </br></br> 3) Harjoitus tulee tehdä harjoitus alueen sisällä. </br></br>";
                return returnText;
            case 2: returnText = "1) Hyökkäävien pelaajien 1 ja 2 tavoite on saada pallo pelatuksi rangaistusalueen sisään niin, että puolustajan ohitus tapahtuu seinäsyötöllä. </br></br> 2) Seinäsyötöllä tarkoitetaan tilannetta jossa toinen pelaaja syöttää toiselle ja saa saman tien syötön takaisin itselleen liikkeeseen. </br></br> 3) Tässä vaiheessa puolustaja harhautuu ja alunperin pallollinen pelaaja pääsee maalinteko tilanteeseen. </br></br>";
                return returnText;
            case 3: returnText = "1) Hyökkäävien pelaajien 1 ja 2 tavoite on saada pallo pelatuksi rangaistusalueen sisään niin, että puolustajan ohitus tapahtuu diagonaalisyötöllä.</br></br> 2) Diagonaalisyötllä tarkoitetaan tilannetta jossa pallollinen pelaaja syöttää puolustajan ohitse toiselle pelaajalle. </br></br> 3) Syötön on tarkoitus suuntautua pystyyn ja palloton pelaaja juoksee pallon kiinni </br></br>";
                return returnText;
            default: returnText = "<h2>Muista valita kenttä ensin!!</h2>";
                return returnText;
            }
        }

        /**
        * Function that will return levels header text
        * If there is no level select will return empty string
        * 
        * @param {number} levelNum is index of level that is played atm
        * @return {string} returnText is header text
        */
        function chooseHeaderText (levelNum) {
            var returnText;
            switch (levelNum){
            case 0: returnText = "Hiekkalaatikko";
                return returnText;
            case 1: returnText = "Takaakierto";
                return returnText;
            case 2: returnText = "Seinäsyöttö";
                return returnText;
            case 3: returnText = "Diagonaalisyöttö";
                return returnText;
            default: returnText = "";
                return returnText;
            }
        }

        /**
        * Function that will start level
        * Clears cogfut-container from UI buttons
        * Removes infoDiv if it is still shown
        * Calls cogfut to start new level
        */
        function playLevel (levelNum) {
            $('#play-Replay-footer-div').empty();
            removeInfoDiv();
            $('#chooseLevelModal').modal('hide');
            window.cogfut.playLevel(levelNum);
        }

        /**
        * Function listener for cogfut.onLevelFinished
        * Sets levelFinished true
        * Sets header text failMsg or succesMesage
        * Adds header text to videoModal header
        * and opens video modal with video of that level
        * also sets watch replay button to visible 
        */
        window.cogfut.onLevelFinished(function (failMsg) {
            var headerText, headerTag;
            var level = window.cogfut.getLevelIndex();
            if(failMsg){
                headerText = chooseHeaderText(level)+' epäonnistui: '+'<p class="text-error">' + failMsg + '</p>';
            }else{
                 headerText =  chooseHeaderText(level) + ' <p class="text-success">onnistui</p> ';
            }
            levelFinsihed = true;
            $('#video-modal-header').empty();
            headerTag = document.createElement('h1');
            headerTag.innerHTML = headerText;
            $('#video-modal-header').append(headerTag);
            $('#video-modal-close-btn').hide();
            $('#video-modal-replay-btn').show();
            $("#video-modal-body").html(
                '<video width="500" height="264" autoplay>' +
                '<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + level + 'video.mp4" type="video/mp4"></source>' +
                '<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + level + 'video.webm" type="video/webm"></source>' +
                '<source src="http://www.cs.helsinki.fi/u/mikivi/cogfut/videos/level' + level + 'video.ogv" type="video/ogg"></source>' +
            '</video>');
            $('#videoModal').modal('show');
        });
    });
});
