define(function (require) {

    var Kinetic = require('kinetic');

    var Const   = require('const');

    /**
     * @name InGameMenu
     *
     * @class Displays a button row, either pop-up or fixed, on a
     * Kinetic stage. Each button is assigned an on-click handler
     * function and can be set to be enabled or disabled when the menu
     * is created. The on-click handler is only invoked for enabled
     * buttons. Enabled buttons are highlighted visually when moused
     * over.
     *
     * Most menus used by the game code are managed using InGameMenu,
     * whereas GUI buttons are done using Bootstrap.
     *
     * @property {Array} shapeList The list of Shape objects representing the buttons.
     * @property {Kinetic.Group} group The Group object used to hold the buttons together.
     * @property {boolean} isPopup Whether the menu is currently shown as a pop-up menu rather than fixed.
     * @constructor
     */
    function InGameMenu() {
        this.shapeList = [];
        this.group = null;
        this.isPopup = false;
    }

    /**
     * Create a new a button at the end of the the menu.
     *
     * @param {string} text The text to display on the button.
     * @param {boolean} enabled Whether or not the button will be enabled.
     * @param {function} command The function to call if the button is clicked.
     */
    InGameMenu.prototype.add = function (text, enabled, command) {
        var shape;
        shape = new Kinetic.Text(enabled ? Const.buttonStyle : Const.disabledButtonStyle);
        shape.setText(text);
        if (enabled) {
            shape.on('click', this.shapeClick.bind(this, shape, command));
            shape.on('mouseover', this.shapeMouseOver.bind(this, shape));
            shape.on('mouseout', this.shapeMouseOut.bind(this, shape));
        }
        this.shapeList.push(shape);
    };

    /**
     * An internal event handler called when a button is clicked.
     *
     * @private
     * @param {Kinetic.Shape} shape The Kinetic shape representing the button.
     * @param {function} command The on-click handler that has been given for the button.
     */
    InGameMenu.prototype.shapeClick = function (shape, command) {
        command();
        if (this.isPopup) {
            this.hide();
        }
    };

    /**
     * An internal event handler called when the mouse pointer enters the area of a button.
     *
     * @private
     * @param {Kinetic.Shape} shape The Kinetic shape representing the button.
     */
    InGameMenu.prototype.shapeMouseOver = function (shape) {
        shape.setFill(Const.buttonMouseOverFill);
        shape.getLayer().draw();
    };

    /**
     * An internal event handler called when the mouse pointer leaves the area of a button.
     *
     * @private
     * @param {Kinetic.Shape} shape The Kinetic shape representing the button.
     */
    InGameMenu.prototype.shapeMouseOut = function (shape) {
        shape.setFill(Const.buttonFill);
        shape.getLayer().draw();
    };

    /**
     * Show the menu on the screen in a fixed position. The menu will
     * remain visible and await further commands once a button has
     * been clicked. This method is also used internally to hide the
     * menu, or as part of showing it as pop-up.
     *
     * @param {Kinetic.Layer} layer The Kinetic layer on which to show the menu.
     * @param {object} position An object with properties x and y giving the upper left corner pixel coordinates for the menu on the Kinetic stage.
     * @return {boolean} Whether the menu was shown rather than hidden.
     */
    InGameMenu.prototype.show = function (layer, position) {
        var oldLayer, shape, s, x, y;
        if (this.group) {
            oldLayer = this.group.getLayer();
            this.group.remove();
            if (oldLayer !== layer) {
                oldLayer.draw();
            }
        }
        this.group = null;
        this.isPopup = false;
        if ((layer === null) || (this.shapeList.length === 0)) {
            return false;
        }
        x = position.x;
        y = position.y;
        this.group = new Kinetic.Group();
        for (s = 0; s < this.shapeList.length; s++) {
            shape = this.shapeList[s];
            shape.setPosition(x, y);
            y += shape.getHeight();
            this.group.add(shape);
        }
        layer.add(this.group);
        layer.draw();
        return true;
    };

    /**
     * Show the menu on the screen as a pop-up menu. The menu will be
     * hidden once an enabled button has been clicked.
     *
     * To hide the menu when the user clicks outside the buttons,
     * which is the expected behavior for pop-up menus in computer
     * user interfaces, you must set mouse-down handlers for the
     * objects in the background and hide the menu explicitly from
     * those event handlers.
     *
     * @param {Kinetic.Layer} layer The Kinetic layer on which to show the menu.
     * @param {object} position An object with properties x and y giving the upper left corner pixel coordinates for the menu on the Kinetic stage.
     */
    InGameMenu.prototype.showPopup = function (layer, position) {
        if (this.show(layer, position)) {
            this.isPopup = true;
        }
    };

    /**
     * Hide the menu if it is currently shown. This does not dispose
     * of the menu object; it can be shown again later.
     */
    InGameMenu.prototype.hide = function () {
        this.show(null, null);
    };

    return InGameMenu;

});
