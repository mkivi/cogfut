define(function (require) {

    var AIPawn     = require('aipawn');
    var Api        = require('api');
    var Ball       = require('ball');
    var Const      = require('const');
    var InGameMenu = require('ingamemenu');
    var Pawn       = require('pawn');
    var PlayerPawn = require('playerpawn');
    var Point      = require('point');

    /**
     * @name Level
     *
     * @class Enables the user to actually play a particular level of
     * the game. Manages and draws the playing field; manages all the
     * game objects (player pawns, AI pawns, the ball) and runs the
     * logic of the game and replays. Replay feedback is also
     * currently generated here.
     *
     * @property {cogfut} cogfut The CogFut object commissioning us.
     * @property {Kinetic.Stage} stage The Kinetic stage we draw on.
     * @property {Kinetic.Layer} backLayer The layer we create on the Kinetic stage to draw on.
     * @property {Kinetic.Layer} mainLayer The layer we create on the Kinetic stage to draw on.
     * @property {Kinetic.Layer} ghostLayer The layer we create on the Kinetic stage to draw on.
     * @property {Kinetic.Layer} menuLayer The layer we create on the Kinetic stage to draw on.
     * @property {InGameMenu|null} activePopupMenu The pop-up menu currently shown on screen, if any.
     * @property {Kinetic.Rect} fieldBoundsRect A rectangle used to display the soccer field.
     * @property {Kinetic.Rect} ruleRect A rectangle used to display the soccer field.
     * @property {Kinetic.Rect} goalRect A rectangle used to display the soccer field.
     * @property {Kinetic.Rect} grassRect A rectangle used to display the soccer field.
     * @property {InGameMenu} buttons The buttons always visible on the game screen, currently just Play Turn.
     * @property {Kinetic.Animation} anim The Kinetic animation object used to run each turn of the game.
     * @property {boolean} replaying Whether we are currently replaying a game as opposed to playing one.
     * @property {Kinetic.Text} replayRuleText1 A text object used to show hints during replay on what player 1 should do.
     * @property {Kinetic.Text} replayRuleText2 A text object used to show hints during replay on what player 2 should do.
     * @property {Kinetic.Text} replayBallText A text object used to show hints during replay on ball control.
     * @property {number} nextRule The zero-based index number of the next rule to check for (all prior ones have been passed successfully).
     * @property {Array} ruleList The list of rules to check for during this level.
     * @property {Array} playerPawnList The list of PlayerPawns that together form the player's team.
     * @property {Array} aiPawnList The list of AIPawns that together form the opposing team.
     * @property {number} turnIndex The zero-based index number of the turn currently being set up or played.
     * @property {Ball} ball The object representing the soccer ball in the game.
     * @property {number} turnCount The total number of turns that have been played so far in the game, including any one currently being set up.
     *
     * @param {CogFut} cogfut The CogFut object commissioning us. Provides the Kinetic stage to draw on.
     */
    function Level(cogfut) {
        this.cogfut = cogfut;
        this.stage = this.cogfut.stage;
        this.backLayer = new Kinetic.Layer();
        this.mainLayer = new Kinetic.Layer();
        this.ghostLayer = new Kinetic.Layer();
        this.menuLayer = new Kinetic.Layer();
        this.stage.add(this.backLayer);
        this.stage.add(this.mainLayer);
        this.stage.add(this.ghostLayer);
        this.stage.add(this.menuLayer);
        this.activePopupMenu = null;
        this.fieldBoundsRect = null;
        this.ruleRect = null;
        this.goalRect = null;
        this.grassRect = null;
        this.makeSoccerField();
        this.grassRect.on('mousedown', this.hidePopupMenu.bind(this));
        this.buttons = null;
        this.anim = null;

        this.replaying = false;

        this.replayRuleText1 = new Kinetic.Text(Const.replayTextStyle);
        this.replayRuleText2 = new Kinetic.Text(Const.replayTextStyle);
        this.replayBallText = new Kinetic.Text(Const.replayTextStyle);

        this.replayRuleText1.setVisible(false);
        this.replayRuleText2.setVisible(false);
        this.replayBallText.setVisible(false);

        this.ghostLayer.add(this.replayRuleText1);
        this.ghostLayer.add(this.replayRuleText2);
        this.ghostLayer.add(this.replayBallText);

        this.nextRule = 0;
        this.ruleList = [];
        this.playerPawnList = [];
        this.aiPawnList = [];
        this.turnIndex = null;
        this.setupLevel();
        if (this.playerPawnList.length === 0) {
            throw 'Level: no player pawns on this level';
        }
        this.ball = new Ball(this, this.playerPawnList[0]);
        this.turnCount = 1;
        this.gotoTurn(0);
    }

    /**
     * Update the row of buttons always visible on the game
     * screen. This must be called manually when a change in the game
     * state requires it. Currently the only visible button is Play
     * Turn. The button is hidden during replay.
     *
     * @private
     */
    Level.prototype.updateButtons = function () {
        if (this.buttons) {
            this.buttons.hide();
        }
        if (this.replaying) {
            return;
        }
        this.buttons = new InGameMenu();
        this.buttons.add('Pelaa siirto',
                         (this.turnIndex < this.turnCount),
                         this.playTurnCommand.bind(this));
        // this.buttons.add('Edellinen',
        //                  (this.turnIndex > 0),
        //                  this.prevTurnCommand.bind(this));
        // this.buttons.add('Seuraava',
        //                  (this.turnIndex < this.turnCount-1),
        //                  this.nextTurnCommand.bind(this));
        this.buttons.show(this.menuLayer,
                          {x: this.stage.getWidth() - Const.inGameMenuMarginPixels - Const.inGameMenuWidthPixels,
                           y: Const.inGameMenuMarginPixels});
    };

    /**
     * Implement the command for the Play Turn button.
     *
     * @private
     */
    Level.prototype.playTurnCommand = function () {
        this.hidePopupMenu();
        this.playTurn();
    };

    /**
     * Implement the command for the Previous Turn button, currently
     * always hidden.
     *
     * @private
     */
    Level.prototype.prevTurnCommand = function () {
        this.hidePopupMenu();
        this.gotoTurn(this.turnIndex-1);
    };

    /**
     * Implement the command for the Next Turn button, currently
     * always hidden.
     *
     * @private
     */
    Level.prototype.nextTurnCommand = function () {
        this.hidePopupMenu();
        this.gotoTurn(this.turnIndex+1);
    };

    /**
     * Show the given pop-up menu on the screen, hiding any previous
     * one. This method is also used internally just to hide the
     * previous menu.
     *
     * @param {InGameMenu} menu The menu to pop up.
     * @param {Kinetic.Shape} shape The shape next to which the menu should appear.
     */
    Level.prototype.showPopupMenu = function (menu, shape) {
        if (this.activePopupMenu) {
            this.activePopupMenu.hide();
        }
        this.activePopupMenu = menu;
        if (this.activePopupMenu) {
            this.activePopupMenu.showPopup(this.menuLayer, shape.getPosition());
        }
    };

    /**
     * Hide the currently shown popup menu, if any.
     */
    Level.prototype.hidePopupMenu = function () {
        this.showPopupMenu(null, null);
    };

    /**
     * Dispose of the level, freeing the Kinetic stage for other
     * use. This is done right before substituting the banner or
     * another level for the current level.
     */
    Level.prototype.dispose = function () {
        this.stage.destroy();
        this.stage = new Kinetic.Stage();
    };

    /**
     * In subclasses, override this to initialize the level's pawns
     * and rules.
     *
     * @private
     */
    Level.prototype.setupLevel = function () {
        // Nothing to do
    };

    /**
     * Add the given rule at the end of the level's rule list.
     *
     * @param {string} objective1 A Finnish description of what player 1 must do to meet this rule.
     * @param {string} objective2 A Finnish description of what player 2 must do to meet this rule.
     * @param {function} checkFunc A function returning a boolean to indicate whether the current game situation matches the rules.
     */
    Level.prototype.addRule = function (objective1, objective2, checkFunc) {
        this.ruleList.push({
            objective1 : objective1,
            objective2 : objective2,
            checkFunc  : checkFunc
        });
    };

    // On collision detection:
    //
    // We could use Kinetic.Container.getIntersections() to do it.
    // However, at least in Chrome it has problems with our ball being
    // an Image from a local file. ('Uncaught Error: SECURITY_ERR: DOM
    // Exception 18': google this for an explanation.)  The current
    // collision test is trivial and works ok for now, but maybe do a
    // better test later?

    /**
     * An internal function to aid in collision detection. Returns the
     * first pawn in the given list that is within the specified range
     * from the ball, or null if there is no such pawn in the list.
     *
     * @private
     * @param {Array} pawnList The list of pawns to consider.
     * @param {number} maxDistance The maximum range to consider.
     * @return {Pawn} The matching pawn, if any.
     */
    Level.prototype.genericPawnTouchingBall = function (pawnList, maxDistance) {
        var pawn, p;
        for (p = 0; p < pawnList.length; p++) {
            pawn = pawnList[p];
            if (Point.distanceBetween(this.ball.point, pawn.point) <= maxDistance) {
                return pawn;
            }
        }
        return null;
    };

    /**
     * Returns the first AI pawn considered to be within reach of
     * dispossessing the ball.
     *
     * @return {AIPawn} The matching pawn, if any.
     */
    Level.prototype.aiPawnTouchingBall = function () {
        return this.genericPawnTouchingBall(this.aiPawnList, Const.maxAIDispossessionDistance);
    };

    /**
     * Returns the first player pawn considered to be within reach of
     * dispossessing the ball.
     *
     * @return {PlayerPawn} The matching pawn, if any.
     */
    Level.prototype.playerPawnTouchingBall = function () {
        return this.genericPawnTouchingBall(this.playerPawnList, Const.maxPlayerDispossessionDistance);
    };


    /**
     * Calls the given mapping function for each game object (first
     * each player pawn in order, then each AI pawn in order, and
     * finally the ball). If the mapping function ever returns a value
     * other than undefined, null, false, then that value is returned
     * to the caller of this function as well and the remaining game
     * objects are not visited. Otherwise undefined is returned. Thus,
     * this function can be used to do things to all game objects as
     * well as to find matches among them.
     *
     * @param {function} func The function to call for each object.
     * @return {object|undefined} The return value from func when it encountered a match, or undefined.
     */
    Level.prototype.eachGameObject = function (func) {
        var p, ans;
        ans = undefined;
        for (p=0; !ans && p<this.playerPawnList.length; p++) {
            ans = func.call(this, this.playerPawnList[p]);
        }
        for (p=0; !ans && p<this.aiPawnList.length; p++) {
            ans = func.call(this, this.aiPawnList[p]);
        }
        if (!ans) {
            ans = func.call(this, this.ball);
        }
        return ans;
    };

    /**
     * Set up the game or replay for the given turn.
     *
     * @private
     * @param {number} turnIndex The zero-based index number of the turn to go to.
     * @throws {string} If turnIndex is out of bounds.
     */
    Level.prototype.gotoTurn = function (turnIndex) {
        if ((turnIndex < 0) || (turnIndex > this.turnCount)) {
            throw 'Level: bad turn index';
        }
        this.turnIndex = turnIndex;
        if (this.turnIndex < this.turnCount) {
            this.eachGameObject(function (obj) {
                obj.beginTurnSetup(this.turnIndex);
            });
        }
        this.updateButtons();
        this.stage.draw();
        if (this.replaying) {
            this.playTurn();
        }
    };

    /**
     * Play the current turn in the game or replay. In the game this
     * should be done once the user has clicked on the Play Turn
     * button to indicate that they are done setting things up. During
     * a replay it can be called immediately after setup as the old
     * turn configuration has then already been retrieved from memory.
     *
     * @private
     */
    Level.prototype.playTurn = function () {
        if (this.anim) {
            // Don't do anything if the last turn is still being animated.
            return;
        }
        this.eachGameObject(function (obj) {
            obj.endTurnSetup();
        });
        this.anim = new Kinetic.Animation({func: this.playTurnFrame.bind(this), node: this.mainLayer});
        this.anim.start();
    };

    /**
     * Compute, render and process the implications of one frame of
     * the game. This function is the heart of the game where
     * everything is tied together. It computes the positions of all
     * game objects, does collision detection, updates realtime
     * feedback when replaying, and monitors winning and losing
     * conditions. To keep things manageable, much of this work is
     * actually delegated to the game objects. The replay feedback is
     * still not adequately forked off to other routines,
     * unfortunately. This function calls the event handler
     * this.cogfut.onLevelFinishedFunc when the game has been won or
     * lost, in addition to making server API calls to win/lose APIs.
     *
     * @private
     * @param kineticFrame An object from Kinetic describing the current animation frame.
     */
    Level.prototype.playTurnFrame = function (kineticFrame) {
        var seconds, finished, failMsg, pawnTouchingBall;

        seconds = kineticFrame.time / 1000;
        if (this.replaying) {
            seconds /= Const.replaySlowdownFactor;
        }

        // Compute game state this turn
        finished = 'turn';
        this.eachGameObject(function (obj) {
            if (obj.updateForFrame(seconds)) {
                finished = null;
            }
        });

        //new Point(Const.ruleRectX, Const.ruleRectY+Const.ruleRectLength+1).assignToShape(this.replayRuleText1);
        //new Point(Const.ruleRectX, Const.ruleRectY+Const.ruleRectLength+3).assignToShape(this.replayRuleText2);
        this.playerPawnList[0].point.assignToShape(this.replayRuleText1);
        this.playerPawnList[1].point.assignToShape(this.replayRuleText2);
        this.replayRuleText1.setVisible(this.replaying && (this.ruleList.length > 0));
        this.replayRuleText2.setVisible(this.replaying && (this.ruleList.length > 0));
        if (this.replaying) {
            // Display on-screen feedback during replay
            // @todo: Unicode right arrow '\u2192'
            if (this.nextRule < this.ruleList.length) {
                this.replayRuleText1.setText(this.ruleList[this.nextRule].objective1);
                this.replayRuleText2.setText(this.ruleList[this.nextRule].objective2);
                this.replayRuleText1.setFill('white');
                this.replayRuleText2.setFill('white');
            } else {
                this.replayRuleText1.setText('Hyvin menee!');
                this.replayRuleText2.setText('Hyvin menee!');
                this.replayRuleText1.setFill('lime');
                this.replayRuleText2.setFill('lime');
            }
        }

        Point.addPoints(this.ball.point, new Point(-5, -2)).assignToShape(this.replayBallText);
        this.replayBallText.setVisible(this.replaying);
        this.replayBallText.setText('Pallon paikka OK');
        this.replayBallText.setFill('lime');

        if (!finished && kineticRectContainsShape(this.goalRect, this.ball.mainShape)) {
            finished = 'game';
            this.replayBallText.setText('Maali!');
            this.replayBallText.setFill('lime');
            if (this.nextRule === this.ruleList.length) {
                failMsg = null;
            } else {
                failMsg = 'harjoituksen tarkistussääntöjä ei täytetty';
            }
        }
        if (!finished && !kineticRectContainsShape(this.fieldBoundsRect, this.ball.mainShape)) {
            finished = 'game';
            failMsg = 'pallo päätyi rajojen ulkopuolelle';
            this.replayBallText.setText('Pallo rajan yli!');
            this.replayBallText.setFill('red');
        }
        if (!finished) {
            pawnTouchingBall = this.aiPawnTouchingBall();
            if (pawnTouchingBall) {
                finished = 'game';
                failMsg = 'vastustaja sai pallon';
                this.replayBallText.setText('Puolustaja sai pallon');
                this.replayBallText.setFill('red');
            }
        }

        if (!finished) {
            if (!(this.ball.turn.source instanceof PlayerPawn)) {
                this.replayBallText.setText('Pallo menetetty!');
                this.replayBallText.setFill('red');
            } else if ((this.aiPawnList.length > 0) &&
                       (Point.distanceBetween(this.ball.point, this.aiPawnList[0].point) < Const.ballMinSafeDistanceFromAI))  {
                if (!this.ball.turn.shield) {
                    this.replayBallText.setText('Varo puolustajaa!');
                    this.replayBallText.setFill('red');
                } else {
                    this.replayBallText.setText('Suojattu: hyvä!');
                    this.replayBallText.setFill('lime');
                }
            }
        }

        this.ghostLayer.draw(); // Display onscreen feedback during replay

        // Finish the turn, and possibly the entire game, if appropriate.
        if (!finished) {
            return;
        }
        this.anim.stop();
        this.anim = null;
        if (finished === 'game') {
            if (!this.replaying) {
                if (failMsg) {
                    Api.lost();
                } else {
                    Api.won();
                }
                if (this.cogfut.onLevelFinishedFunc) {
                    this.cogfut.onLevelFinishedFunc(failMsg);
                }
            }
            return;
        }
        // @todo: Currently we don't guard against entering ruleRect a
        // second time and complying with the rules then, if the
        // player failed to comply the first time...
        if ((this.nextRule < this.ruleList.length)
            && kineticRectContainsShape(this.ruleRect, this.ball.mainShape)
            && this.ruleList[this.nextRule].checkFunc.call(this)) {
            // Rule passed!
            this.nextRule++;
        }
        if (!this.replaying && (this.turnIndex === this.turnCount-1)) {
            this.eachGameObject(function (obj) {
                obj.appendTurn();
            });
            this.turnCount++;
        }
        this.gotoTurn(this.turnIndex+1);
    };

    /**
     * Launch a replay of the current level.
     */
    Level.prototype.playReplay = function () {
        this.replaying = true;
        this.nextRule = 0;
        this.gotoTurn(0);
    };

    /**
     * Draw the grass and goal and paint the lines of the soccer
     * field. Dimensions come from the Const class.
     *
     * @private
     */
    Level.prototype.makeSoccerField = function () {
        var layer;

        function makeRect(x, y, w, h, stroke) {
            var rect;
            rect = new Kinetic.Rect({
                x           : Point.pixelsFromUnits(x),
                y           : Point.pixelsFromUnits(y),
                width       : Point.pixelsFromUnits(w),
                height      : Point.pixelsFromUnits(h),
                strokeWidth : Const.fieldLineThicknessPixels,
                stroke      : stroke});
            layer.add(rect);
            return rect;
        }

        layer = this.backLayer;

        Point.pixelsPerUnit =
            Math.min(this.stage.getWidth() / Const.fullWidth,
                     this.stage.getHeight() / Const.fullLength);

        // Grass
        this.grassRect =
            layer.add(new Kinetic.Rect({
                x:0,
                y:0,
                width:layer.getStage().getWidth(),
                height:layer.getStage().getHeight(),
                fill:'green'}));

        // Endline, sidelines and center line
        this.fieldBoundsRect =
            makeRect(Const.fieldMargin,
                     Const.endlineMargin,
                     Const.fieldWidth,
                     Const.fieldLength - Const.fieldMargin - Const.endlineMargin,
                     'white');

        // Penalty area
        makeRect(Const.fieldMargin + (Const.fieldWidth-Const.penaltyAreaWidth)/2,
                 Const.endlineMargin,
                 Const.penaltyAreaWidth,
                 Const.penaltyAreaLength,
                 'white');

        // Goal periphery
        makeRect(Const.fieldMargin + (Const.fieldWidth-Const.goalWidth)/2 - Const.goalPeriphery,
                 Const.endlineMargin,
                 Const.goalWidth + 2*Const.goalPeriphery,
                 Const.goalPeriphery,
                 'white');

        // Goal
        this.goalRect =
            makeRect(Const.fieldMargin + (Const.fieldWidth-Const.goalWidth)/2,
                     Const.fieldMargin,
                     Const.goalWidth,
                     Const.goalLength,
                     'silver');

        // Center circle
        layer.add(new Kinetic.Shape({
            drawFunc: function (context) {
                var x, y, radius, startAngle, endAngle, counterClockwise;
                x = Point.pixelsFromUnits(Const.fieldMargin + Const.fieldWidth/2);
                y = Point.pixelsFromUnits(Const.endlineMargin + Const.fieldLength);
                radius = Point.pixelsFromUnits(Const.fieldCircleRadius);
                startAngle = 1 * Math.PI;
                endAngle = 0 * Math.PI;
                counterClockwise = false;
                context.beginPath();
                context.arc(x, y, radius, startAngle, endAngle, counterClockwise);
                context.stroke();
                context.closePath();
                this.stroke(context);
            },

            // @todo: This is incorrect but looks ok visually...
            offset: [0,
                     Point.pixelsFromUnits(Const.fieldCircleRadius - Const.fieldCircleRadius/3) +
                     Const.fieldLineThicknessPixels/2],

            stroke: 'white',
            strokeWidth: Const.fieldLineThicknessPixels
        }));

        // Rectangle inside which AI player must be passed in order to
        // comply with the rules of the exercise.
        this.ruleRect =
            makeRect(Const.ruleRectX,
                     Const.ruleRectY,
                     Const.ruleRectWidth,
                     Const.ruleRectLength,
                     'white');

    };

    /**
     * An internal function to aid in collision detection. Checks
     * whether the given Kinetic rectangle "contains" the given
     * Kinetic shape. Currently the test is actually whether the x and
     * y coordinates of the shape fall inside the rectangle.
     *
     * @private
     * @param {Kinetic.Rect} The rectangle 
     * @param {Kinetic.Shape}
     * @return {boolean} Whether the rectangle contains the shape.
     */
    function kineticRectContainsShape(rect, shape) {
        var x, y, x0, y0, x1, y1;
        x = shape.getX();
        y = shape.getY();
        x0 = rect.getX();
        y0 = rect.getY();
        x1 = x0 + rect.getWidth();
        y1 = y0 + rect.getHeight();
        return ((x0 <= x) && (x <= x1) &&
                (y0 <= y) && (y <= y1));
    }

    return Level;

});
