define(function (require) {

    var Point = require('point');

    /**
     * @name Const
     *
     * @class Collects the tweakable constants in the game in one
     * place.
     */
    var Const = {};

    //--- Soccer field dimensions (in point units, i.e. meters)

    // http://www.soccer-fans-info.com/image-files/soccer-field-layout.jpg

    // http://upload.wikimedia.org/wikipedia/en/thumb/9/96/Football_pitch_metric_and_imperial.svg/1000px-Football_pitch_metric_and_imperial.svg.png

    Const.penaltyAreaLength = 16.5;

    Const.penaltyAreaWidth = 40.3;

    Const.penaltyArcEndlineDistance = 11;

    Const.goalLength = 2.44;

    Const.goalWidth = 7.32;

    Const.goalPeriphery = 5.5;

    Const.fieldCircleRadius = 9.5;

    Const.fieldLength = 60;

    Const.fieldWidth = 70; // This can be anything from 45 to 90, according to the specs.

    Const.fieldMargin = 2;

    Const.endlineMargin = Const.fieldMargin + Const.goalLength;

    Const.fieldLineThicknessPixels = 4;

    Const.fieldCenterPointX = Const.fieldMargin + Const.fieldWidth/2;

    Const.fieldCenterPointY = Const.endlineMargin + Const.fieldLength;

    Const.ruleRectWidth = 20;

    Const.ruleRectLength = 20;

    Const.ruleRectX = Const.fieldMargin + (Const.fieldWidth-Const.ruleRectWidth)/2;

    Const.ruleRectY = Const.endlineMargin + Const.penaltyAreaLength + 4;

    Const.fullWidth = Const.fieldWidth + Const.fieldMargin + Const.fieldMargin;

    Const.fullLength = Const.fieldLength + Const.fieldMargin + Const.endlineMargin;

    Const.windowSizeFudgePixels = -20;

    //--- Game distances (in point units)

    Const.maxPlayerDispossessionDistance = 4;

    Const.maxAIDispossessionDistance = 3;

    Const.pawnMoveRangeWithoutBall = 10;

    Const.pawnMoveRangeWithBall = 7.5;

    // AI pawn's moving range per turn
    Const.aiPawnMoveRange = 8;

    Const.ballMoveRange = 20;

    Const.ballSnapDistance = 2;

    Const.pawnSnapDistance = 1.25;

    Const.ballMinSafeDistanceFromAI = 7;

    //--- Game speeds (in point units per second)

    Const.pawnSpeedWithoutBall = 15;

    Const.pawnSpeedWithBall = 15;

    Const.ballPassSpeed = 23;

    Const.ballShotSpeed = 40;

    Const.replaySlowdownFactor = 2;

    //--- Game shape styles

    Const.pawnRadiusPixels = 18;

    Const.pawnMainShapeStyle = {
        radius: Const.pawnRadiusPixels,
        sides: 3,
        strokeWidth: 2,
        stroke: 'black'
    };

    Const.pawnGhostShapeStyle = {
        radius: Const.pawnRadiusPixels,
        sides: 3,
        strokeWidth: 2,
        stroke: 'black',
        fill: 'red',
        opacity: 0.3
    };

    Const.pawnGhostRangeStyle = {
        fill: 'blue',
        stroke: 'black',
        strokeWidth: 4,
        opacity: 0.1
    };

    Const.ballGhostRangeStyle = {
        fill: 'yellow',
        stroke: 'black',
        strokeWidth: 4,
        opacity: 0.1
    };

    Const.aiMoveRangeStyle = {
        fill: 'black',
        stroke: 'black',
        strokeWidth: 4,
        opacity: 0.1
    };

    Const.passLineStyle = {
        points: [0, 0, 0, 0],
        stroke: 'yellow',
        strokeWidth: 1,
        visible: false
    };

    //--- User interface widgets

    Const.inGameMenuMarginPixels = 15;

    Const.inGameMenuWidthPixels = 120;

    Const.buttonFill = '#ddd';

    Const.buttonMouseOverFill = '#0affff';

    Const.buttonStyle = {
        width: Const.inGameMenuWidthPixels,
        fontSize: 10,
        fontFamily: 'sans-serif',
        textFill: 'black',
        stroke: '#555',
        strokeWidth: 2,
        padding: 5,
        align: 'center',
        fill: Const.buttonFill,
        cornerRadius: 4
    };

    Const.disabledButtonStyle = {
        width: Const.inGameMenuWidthPixels,
        fontSize: 10,
        fontFamily: 'sans-serif',
        textFill: '#fff',
        stroke: '#555',
        strokeWidth: 2,
        padding: 5,
        align: 'center',
        fill: '#aaa',
        cornerRadius: 4
    };

    Const.replayTextStyle = {
        fontSize: 10,
        fontFamily: 'sans-serif',
        textFill: 'black',
        stroke: '#555',
        strokeWidth: 2,
        padding: 5,
        align: 'center',
        fill: 'white',
        cornerRadius: 4
    };

    //--- Images

    Const.images = {
        ballGhost:  'img/1349704073_football_gray.png',
        ballMain:   'img/1349704073_football_white.png',
        charBlueP:  'img/charBlueP.png',
        charRed1:   'img/charRed1.png',
        charRed2:   'img/charRed2.png',
        cogfutLogo: 'img/cogfutLogo.png'
    };

    Const.aiPawnImages = [
        Const.images.charBlueP
    ];

    Const.playerPawnImages = [
        Const.images.charRed1,
        Const.images.charRed2
    ];

    return Const;

});
