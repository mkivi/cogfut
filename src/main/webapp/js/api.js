define(function (require) {

    var $ = require('jquery');

    /**
     * @name Api
     *
     * @class Provides functions that make API calls to the server. If
     * the server is not available and we are running from a local
     * file instead, the server calls return the same results as when
     * the server is available but experiencing connection problems.
     */
    var Api = {};

    /**
     * Determine whether or not we can connect to the server to use
     * the API.
     *
     * @return {boolean} True if the server is available, false if not.
     */
    Api.canUseApi = function () {
        return window.location.host && (window.location.host !== '');
    }

    /**
     * Return a URL created by joining together the given components.
     * The URL is absolute and contains host and protocol information.
     * The components are URL-encoded and delimited by forward-slashes
     * as if traversing a directory hierarchy.
     *
     * @param {array} urlParts An array of URL components, which are strings.
     * @return {string} The URL.
     */
    function apiUrl (urlParts) {
        var url, i;
        if (window.location.host.toLowerCase().indexOf('heroku') !== -1) {
            url = 'http://'+window.location.host+'/api';
        } else {
            url = 'http://'+window.location.host+'/cogfut/api';
        }
        for (i=0; i<urlParts.length; i++) {
            url += '/'+encodeURIComponent(urlParts[i]);
        }
        return url;
    }

    /**
     * Actually call a server API, given the salient details on how to
     * call it. Once the server responds, call the user-supplied
     * callback function. This function returns before the callback is
     * called.
     *
     * @param {function} callback The callback. It gets one argument, which is the parsed JSON response or undefined on server failure.
     * @param {Array} urlParts A list of URL components from which the API URL is constructed. The server host and its api directory are implicitly added.
     * @param {string} type HTTP request method to use, either 'GET' or 'POST' as specified by the API.
     * @param {object} data An object whose fields are key-value pairs used as GET or POST parameters to the API call.
     */
    function callApi (callback, urlParts, type, data) {
        callback = callback || function (response) {};
        if (!Api.canUseApi()) {
            callback(undefined);
            return;
        }
        $.ajax({
            url: apiUrl(urlParts),
            type: type,
            data: data,
            // dataType needs to be set to 'json' explicitly because
            // if jQuery tries to auto-detect the data type it thinks
            // a zero-length response from the server is XML!
            dataType: 'json',
            success: function(data, status) {
                callback(data);
            },
            error: function() {
                callback(undefined);
            }
        });
    }

    /**
     * Return the username of the user that is currently signed
     * in. Returns '' if no user is signed in.
     *
     * @return {string} The username or ''.
     */
    function getUsername () {
        var username;
        username = localStorage.getItem('username');
        if (!username) {
            return '';
        }
        return username;
    }

    /**
     * Sign up for a new user account on the server.
     *
     * @param {string} username The desired username.
     * @param {string} password The password to use.
     * @param {number} age The user's self-reported age in years.
     * @param {function} callback The function to call with the response from the server.
     */
    Api.register = function (username, password, age, callback) {
        callApi(callback, ['register'], 'POST', {
            username: username,
            password: password,
            age:      age
        });
    };

    /**
     * Sign into an existing user account on the server.
     *
     * @param {string} username The desired username.
     * @param {string} password The password to use.
     * @param {function} callback The function to call with the response from the server.
     */
    Api.login = function (username, password, callback) {
        callApi(callback, ['login'], 'POST', {
            username: username,
            password: password
        });
    };

    /**
     * Sign out of the current user account.
     *
     * @param {function} callback The function to call with the response from the server.
     */
    Api.logout = function (callback) {
        callApi(callback, ['logout'], 'GET', {});
    };

    /**
     * Retrieve profile information for the user that is currently
     * logged in.
     *
     * @param {function} callback The function to call with the response from the server.
     */
    Api.profile = function (callback) {
        callApi(callback, ['profile', getUsername()], 'GET', {});
    };

    /**
     * Tell the server that the current user just won a level in the
     * game.
     */
    Api.won = function () {
        callApi(null, ['won', getUsername()], 'POST', {});
    };

    /**
     * Tell the server that the current user just lost a level in the
     * game.
     */
    Api.lost = function () {
        callApi(null, ['lost', getUsername()], 'POST', {});
    };

    return Api;

});
