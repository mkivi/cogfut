define(function (require) {

    var Kinetic = require('kinetic');

    var Assets  = require('assets');
    var Const   = require('const');
    var Point   = require('point');

    /**
     * @name Banner
     *
     * @class Displays a banner at startup and between levels to set
     * the mood for the game. The banner shows the title of the game
     * in a big font as well as some supplementary graphics. The
     * display is currently static and not interactive. It is
     * implemented using Kinetic JS.
     *
     * @property {cogfut} cogfut The CogFut object commissioning us.
     * @property {Kinetic.Stage} stage The Kinetic stage we draw on.
     * @property {Kinetic.Layer} mainLayer The layer we create on the Kinetic stage to draw on.
     * @property {Kinetic.Circle} ball A big simplified representation of a soccer ball.
     * @property {Kinetic.Image} logoText The CogFut logo text as an image.
     *
     * @param {CogFut} cogfut The CogFut object commissioning us. Provides the Kinetic stage to draw on.
     */
    function Banner(cogfut) {
        this.cogfut = cogfut;
        this.stage = this.cogfut.stage;
        this.mainLayer = new Kinetic.Layer();

        // The background that fills the stage
        this.mainLayer.add(new Kinetic.Rect({
            x: 0,
            y: 0,
            width: this.stage.getWidth(),
            height: this.stage.getHeight(),
            fill: 'green'
        }));

        this.ball = new Kinetic.Circle({
            radius: 70,
            fill: 'white',
            stroke: 'darkgreen',
            strokeWidth: 3
        });
        this.ball.setX(this.stage.getWidth() / 2);
        this.ball.setY(this.stage.getHeight() / 2);
        this.mainLayer.add(this.ball);

        this.logoText = new Kinetic.Image(Assets.getImageAttrs(Const.images.cogfutLogo));
        this.logoText.setX(this.stage.getWidth() / 2);
        this.logoText.setY(this.ball.getY() - this.ball.getRadius() - this.logoText.getHeight());
        this.mainLayer.add(this.logoText);

        this.stage.add(this.mainLayer);
    }

    /**
     * Dispose of the banner, freeing the Kinetic stage for other
     * use. This is done right before substituting a level for the
     * banner.
     */
    Banner.prototype.dispose = function () {
        this.stage.destroy();
        this.stage = new Kinetic.Stage();
    };

    return Banner;

});
