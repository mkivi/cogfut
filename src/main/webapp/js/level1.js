define(function (require) {

    var AIPawn     = require('aipawn');
    var Const      = require('const');
    var Level      = require('level');
    var Pawn       = require('pawn');
    var PlayerPawn = require('playerpawn');
    var Point      = require('point');

    /**
     * @name Level1
     *
     * @class Implements level one of the game. There are two player
     * pawns and one opponent. P1 must pass the ball to P2, who
     * meanwhile has run to the left of P1 and charges past the
     * opponent from the left.
     */
    function Level1(cogfut) {
        this.super = this.__proto__.__proto__;
        this.super.constructor.call(this, cogfut);
    };

    Level1.prototype.__proto__ = Level.prototype;

    /**
     * Make the gamepieces and rules for this level.
     */
    Level1.prototype.setupLevel = function () {

        this.pawn1 = new PlayerPawn(this, new Point(-5, -15));
        this.pawn2 = new PlayerPawn(this, new Point(+5, -15));
        this.pawnP = new AIPawn(this, new Point(0, -40));

        this.addRule('etene oikealle', 'kierrä 1 vasemmalta', function () {
            return ((this.ball.turn.source === this.pawn1) &&
                    (this.pawn2.point.x < this.pawn1.point.x))
        });

        this.addRule('syötä 2:lle', 'ohita P', function () {
            return ((this.ball.turn.source === this.pawn2) &&
                    (this.pawn2.point.x < this.pawnP.point.x) &&
                    (this.pawnP.point.x < this.pawn1.point.x));
        });

    }

    return Level1;

});
