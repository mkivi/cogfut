define(function (require) {

    var Kinetic    = require('kinetic');

    var Assets     = require('assets');
    var Const      = require('const');
    var InGameMenu = require('ingamemenu');
    var Motion     = require('motion');
    var Pawn       = require('pawn');
    var Point      = require('point');

    /**
     * @name PlayerPawn
     *
     * @class Represents a user-controlled player on the friendly
     * team. Subclass of the Pawn class.
     * 
     * @property {boolean} hasPassed Boolean to know if this Pawn has passed the ball (if we have dragged the ball away from it) at current turn.
     * @property {Kinetic.Image} mainShape
     * @property {Kinetic.Image} ghostShape
     * @property {Kinetic.Circle} ghostRange
     *
     * @constructor
     * @param {Level} level The level object that commissions us. We can access the Kinetic stage and all other game objects through this.
     * @param {Point} initialOffset The location of the pawn at the beginning of the level.
     */
    function PlayerPawn(level, initialOffset) {
        this.hasPassed = false;
        this.super = this.__proto__.__proto__;
        this.super.constructor.call(this, level, initialOffset);
        this.mainShape = new Kinetic.RegularPolygon(Const.pawnMainShapeStyle);
        this.mainShape.setFill(Assets.getImageAttrs(Const.playerPawnImages[this.level.playerPawnList.length]));
        this.mainShape.on('click', this.mainShapeClick.bind(this));
        this.mainShape.on('mouseover', this.mainShapeMouseOver.bind(this));
        this.ghostShape = new Kinetic.RegularPolygon(Const.pawnGhostShapeStyle);
        this.ghostShape.setVisible(false);
        this.ghostShape.setDraggable(true);
        this.ghostShape.setDragBoundFunc(this.ghostShapeDragBoundFunc.bind(this));
        this.ghostShape.on('mouseover', this.ghostShapeMouseOver.bind(this));
        this.ghostShape.on('dragstart', this.ghostShapeDragStart.bind(this));
        this.ghostShape.on('dragend', this.ghostShapeDragEnd.bind(this));
        this.ghostShape.on('click', this.ghostShapeClick.bind(this));
        this.ghostRange = new Kinetic.Circle(Const.pawnGhostRangeStyle);
        this.ghostRange.setVisible(false);
        this.ghostRange.on('mouseout', this.ghostRangeMouseOut.bind(this));
        this.setMoveRange(Const.pawnMoveRangeWithoutBall);
        this.level.mainLayer.add(this.mainShape);
        this.level.mainLayer.add(this.ghostShape);
        this.level.mainLayer.add(this.ghostRange);
        this.level.playerPawnList.push(this);
    }

    PlayerPawn.prototype.__proto__ = Pawn.prototype;

    /**
     * This method is called at the beginning of the "setup phase" of
     * each turn. The setup phase is the phase where you can move the
     * pawns and the ball, etc. The setup phase ends when the "Play"
     * button is clicked.
     *
     * @param {number} turnIndex The zero-based index of the turn that is beginning
     */
    PlayerPawn.prototype.beginTurnSetup = function (turnIndex) {
        this.turn = this.turnList[turnIndex];
        this.setPoint(this.turn.sourcePoint);
        this.setMoveRange(Const.pawnMoveRangeWithoutBall);
        this.turn.targetPoint.assignToShape(this.ghostShape);
        this.ghostShape.setVisible(!this.turn.frozen);
    }

    /**
     * Shows popup menu when clicking the main shape
     */
    PlayerPawn.prototype.mainShapeClick = function () {
        if (!this.turn.frozen) {
            this.level.showPopupMenu(this.buildMenu(), this.mainShape);
        }
    }

    /**
     * Shows Pawn's move range and ghost shape when hovering mouse over
     */
    PlayerPawn.prototype.mainShapeMouseOver = function () {
        this.ghostRange.show();
        this.ghostShape.show();
        this.ghostShape.moveToTop();
        this.ghostRange.moveToBottom();
        this.level.mainLayer.draw();
    }

    /**
     * shows Pawn's move range when hovering mouse over the ghost shape
     */
    PlayerPawn.prototype.ghostShapeMouseOver = function () {
        this.ghostRange.show();
        this.ghostRange.moveToBottom();
        this.level.mainLayer.draw();
    }

    /**
     * Shows Pawn's move range if it's not visible when starting to drag ghost shape
     */
    PlayerPawn.prototype.ghostShapeDragStart = function () {
        if (!this.ghostRange.isVisible()){
            this.ghostRange.show();
            this.ghostRange.getLayer().draw();
        }
    }

    /**
     * This function is called everytime ghostShape moves. It blocks
     * moving when going over moving range. Updates passing line if
     * wallpassing.
     *
     * @param {object} pos Ghost shape's point in pixels
     * @return {object} Ghost shape's point in pixels after we have made sure 
     * it's inside the move range
     */
    PlayerPawn.prototype.ghostShapeDragBoundFunc = function (pos) {
        var x, y, dx, dy, len, scale;
        x = Point.pixelsFromUnits(this.point.x);
        y = Point.pixelsFromUnits(this.point.y);
        dx = pos.x - x;
        dy = pos.y - y;
        len = Math.sqrt(dx*dx + dy*dy); // Pythagoras' theorem
        scale = Point.pixelsFromUnits(this.moveRange) / len;
        if(scale < 1) {
            pos = {
                x: x + dx*scale,
                y: y + dy*scale
            };
        }
        this.turn.targetPoint = Point.fromShape(this.ghostShape);
        this.level.ball.updateVisualsIfWallPass();
        return pos;
    }

    /**
     * Hides Pawn's move range and calculates if we want to revert movement.
     */
    PlayerPawn.prototype.ghostShapeDragEnd = function () {
        if (Point.distanceBetween(this.turn.sourcePoint,
                                  this.turn.targetPoint)
            < Const.pawnSnapDistance) {
            this.setPoint(this.turn.sourcePoint);
            this.turn.targetPoint = this.turn.sourcePoint.clone();
        }
        this.ghostRange.hide();
        this.level.mainLayer.draw();
    }

    /**
     * Hides Pawn's ghost range.
     */
    PlayerPawn.prototype.ghostRangeMouseOut = function () {
        this.ghostRange.hide();
        this.level.mainLayer.draw();
    }

    /**
     * Shows popup menu when clicking the ghost shape
     */    
    PlayerPawn.prototype.ghostShapeClick = function () {
        if (!this.turn.frozen) {
            this.level.showPopupMenu(this.buildMenu(), this.ghostShape);
        }
    }

    /**
     * Builds menu for the Pawn
     *
     * @private
     * @return {InGameMenu} Menu with disabled or enabled options 
     * for take over and shield depending on the situation.
     */
    PlayerPawn.prototype.buildMenu = function () {
        var menu;
        menu = new InGameMenu();
        menu.add('Ota haltuun',
                 this.takeOverCommandEnabled(),
                 this.takeOverCommand.bind(this));
        menu.add('Suojaus',
                 this.shieldCommandEnabled(),
                 this.shieldCommand.bind(this));
        return menu;
    }

    /**
     * Method is called when dragging the ball ends. Decreases move range by
     * 2 points and moves ghost shape inside the move range if necessary. This can be
     * the case if we have dragged ghost shape before passing and decreasing the move range.
     */
    PlayerPawn.prototype.passTheBall = function() {
        var point,distance;
        if(!this.hasPassed){
            this.hasPassed = true;
            this.setMoveRange(this.moveRange-2);
        }
        distance = Point.distanceBetween(Point.fromShape(this.ghostShape),this.turn.sourcePoint);
        if(distance > this.moveRange){
            this.moveGhostshapeInsideTheMoverange();
        }
        this.level.mainLayer.draw();
    }

    /**
     * This method must be called when dragging ball's ghost shape back to 
     * on its main shape. Reverts changes made by the passTheBall method.
     */
    PlayerPawn.prototype.revertPass = function() {
        if(this.hasPassed){
            this.hasPassed = false;
            this.setMoveRange(this.moveRange+2);
        }
    }

    /**
     * Checks if the take over is enabled in the popup menu.
     * 
     * @return {boolean} Enabled
     */
    PlayerPawn.prototype.takeOverCommandEnabled = function () {
        return !this.hasBall() && (Point.distanceBetween(this.point, this.level.ball.point) < Const.maxPlayerDispossessionDistance);
    }

    /**
     * This method is called from the popup menu. 
     * Takes ball over if it's loose and close enough to the pawn.
     *
     * @private
     */
    PlayerPawn.prototype.takeOverCommand = function () {
        var ball, distance;
        this.setMoveRange(Const.pawnMoveRangeWithBall);
        ball = this.level.ball;
        ball.turn.source = this;
        ball.setPoint(this.calcPointInFront(ball.mainShape));

        distance = Point.distanceBetween(Point.fromShape(this.ghostShape),this.turn.sourcePoint);
        if(distance > this.moveRange){
            this.moveGhostshapeInsideTheMoverange();
        }

        ball.ghostShape.show();
        ball.ghostShape.moveToTop();
        ball.ghostShape.getLayer().draw();
        ball.mainShape.getLayer().draw();
    }

    /**
     * Checks if the shield command is enabled in the popup menu.
     * 
     * @return {boolean} Enabled
     */
    PlayerPawn.prototype.shieldCommandEnabled = function () {
        return this.hasBall() && (this.level.aiPawnList.length > 0);
    }

    /**
     *  This method is called from the popup menu if the pawn got the ball.
     *  This is an on/off method so if the pawn isn't shielding then it will and
     *  vice versa. Updates also ball's passing line if necessary.
     *
     * @private
     */
    PlayerPawn.prototype.shieldCommand = function () {
        var ball, point, ghostpoint, passPoints, distance;
        ball = this.level.ball;
        ghostpoint = Point.fromShape(ball.ghostShape);
        
        if (ball.turn.shield){
            point = this.calcPointInFront(ball.mainShape);
            ball.turn.shield = false;
        } else {
            point = this.calcShieldingPoint();
            ball.turn.shield = true;
        }
        ball.setPoint(point);
        
        if (this.hasPassed){
            passPoints = ball.passLine.getPoints();
            passPoints[0].x = ball.mainShape.getX();
            passPoints[0].y = ball.mainShape.getY();

            distance = Point.distanceBetween(ghostpoint,Point.fromShape(ball.mainShape));
            if(distance > Const.ballMoveRange){
                point = point.nUnitsToward(ghostpoint,Const.ballMoveRange);
                point.assignToShape(ball.ghostShape);
                passPoints[1].x = ball.ghostShape.getX();
                passPoints[1].y = ball.ghostShape.getY();
            }
            else { 
                ghostpoint.assignToShape(ball.ghostShape);
            }
            ball.passLine.setPoints(passPoints);
        }

        ball.ghostShape.getLayer().draw();
        ball.mainShape.getLayer().draw();
    }

    /**
     * This method is called at the end of the "setup phase" of each
     * turn, just before the animation is started.
     */
    PlayerPawn.prototype.endTurnSetup = function () {
        this.hasPassed = false;
        this.ghostShape.hide();
        this.ghostShape.moveToBottom();
        this.ghostRange.hide();
        this.level.mainLayer.draw();
        this.motion = null;
        if (!this.turn.sourcePoint.equals(this.turn.targetPoint)) {
            this.motion = new Motion(this.turn.sourcePoint,
                                     this.turn.targetPoint,
                                     this.hasBall()
                                     ? Const.pawnSpeedWithBall
                                     : Const.pawnSpeedWithoutBall);
        }
    }

    /**
     * This method is called once for each animation frame while a
     * turn is being animated. It computes the position of the object
     * during that frame and moves the object to that position on the
     * screen as well. It returns true if the object may still move,
     * or false if the object has reached the end of its motion for
     * this turn.
     *
     * @param {number} seconds Kinetic frame's starting seconds
     * @return {boolean} Are we still moving
     */
    PlayerPawn.prototype.updateForFrame = function (seconds) {
        if (this.motion === null) {
            return false;
        }
        if (!this.motion.updateForFrame(seconds)) {
            return false;
        }
        this.setPoint(this.motion.point);
        this.mainShape.setRotation(this.motion.angle);
        this.ghostShape.setRotation(this.motion.angle);
        return true;
    }

    /**
     * Calculates the point for the ball in the front of the pawn.
     *
     * @private
     * @param  {Kinetic.Image} offsetShape the ball
     * @return {Point} Calculated point for the ball
     */
    PlayerPawn.prototype.calcPointInFront = function (offsetShape) {
        var x0, y0, len, angle, xnew, ynew;
        x0 = this.mainShape.getX();
        y0 = this.mainShape.getY();
        len = Const.pawnMainShapeStyle.radius + offsetShape.getOffset().x; // @todo: Make this more general and robust.
        angle = this.mainShape.getRotation() - Math.PI/2;
        xnew = x0 + len*Math.cos(angle);
        ynew = y0 + len*Math.sin(angle);
        return Point.fromPixels(xnew, ynew);
    }

    /**
     * Calculates the point for the ball when shielded.
     *
     * @private
     * @return {Point} Calculated point for the ball
     */
    PlayerPawn.prototype.calcShieldingPoint = function () {
        var x0, y0, len, angle, xnew, ynew, aiPawn;
        aiPawn = this.level.aiPawnList[0];
        x0 = this.mainShape.getX();
        y0 = this.mainShape.getY();
        len = Const.pawnMainShapeStyle.radius*3/4;
        angle = Point.angleBetween(aiPawn.point, this.point);
        xnew = x0 + len*Math.cos(angle);
        ynew = y0 + len*Math.sin(angle);
        return Point.fromPixels(xnew, ynew);
    }

    /**
     * Rotates the pawn towards the ball. Currently not used.
     */
    PlayerPawn.prototype.faceBall = function () {
        var angle;
        if (this.hasBall() && this.level.ball.turn.shield) {
            return;
        }
        angle = Point.angleBetween(this.level.ball.point, this.point) - Math.PI/2;
        this.mainShape.setRotation(angle);
        this.ghostShape.setRotation(angle);
    }

    /**
     * Returns true if the pawn has the ball
     * 
     * @return {Boolean} has the ball
     */
    PlayerPawn.prototype.hasBall = function () {
        return this.level.ball.turn.source === this;
    }

    /**
     * Sets point for Pawn
     * @param {Point} newPoint
     */
    PlayerPawn.prototype.setPoint = function (newPoint) {
        this.point = newPoint.clone();
        this.point.assignToShape(this.mainShape);
        this.point.assignToShape(this.ghostRange);
        this.point.assignToShape(this.ghostShape);
    }

    /**
     * Sets new move range
     * @param {number} newMoveRange Radius for the move range in points
     */
    PlayerPawn.prototype.setMoveRange = function (newMoveRange) {
        this.moveRange = newMoveRange;
        this.ghostRange.setRadius(Point.pixelsFromUnits(this.moveRange));
        // @todo: when to redraw?
    }

    /**
     * Moves ghost shape inside the move range if it's outside
     */
    PlayerPawn.prototype.moveGhostshapeInsideTheMoverange = function() {
        var point;
        point = Point.fromShape(this.ghostShape);
        point = this.point.nUnitsToward(point,this.moveRange);
        point.assignToShape(this.ghostShape);
        this.turn.targetPoint = point;
    };

    /**
     * Returns the direction in which we are going to be moving this
     * turn. If we are going to be standing still, just returns the
     * current direction in which we are facing.
     *
     * @private
     * @return {float} The direction as an angle in radians
     */
    PlayerPawn.prototype.calcAngleThisTurn = function () {
        if (!this.turn.sourcePoint.equals(this.turn.targetPoint)) {
            return Point.angleBetween(this.turn.sourcePoint, this.turn.targetPoint);
        } else {
            return this.mainShape.getRotation() - Math.PI/2;
        }
    };

    /**
     * Returns the point at which the ball will lie at the end of the
     * turn if we possess it then. (E.g. if we were dribbling, or if
     * we caught the ball at the end of a wall pass, and the AI didn't
     * catch it first.)  This point will be at the front of the pawn,
     * in the direction in which the pawn will be facing. NOTE: This
     * function doesn't check whether we will actually have the ball.
     *
     * @return {Point} The point in front of the pawn where the ball should lie.
     */
    PlayerPawn.prototype.calcBallTargetPoint = function () {
        return this.turn.targetPoint.nUnitsInDirection(
            this.calcAngleThisTurn(),
            Point.unitsFromPixels(Const.pawnMainShapeStyle.radius));
    };

    return PlayerPawn;

});
