define(function (require) {

    var Kinetic = require('kinetic');

    var Assets  = require('assets');
    var Const   = require('const');
    var Motion  = require('motion');
    var Point   = require('point');

    /**
     * @name Pawn
     *
     * @class Abstract base class for AI and player pawns. Manages the
     * basic turn data structure. Everything else is done in
     * subclasses.
     *
     * @property {object} turn A data structure describing the current turn. This data structure is also contained in turnList.
     * @property {Array} turnList An ordered list of data structures describing the state and actions of this object during each turn of the game.
     * @property {Level} level The level being played.
     * @property {Point} point The current location of this object on the level.
     * @property {Motion|null} motion The current motion of this object if it is moving during an animation.
     *
     * @constructor
     * @param {Level} level The level object that commissions us. We can access the Kinetic stage and all other game objects through this.
     * @param {Point} initialOffset The location of the pawn at the beginning of the level.
     */
    function Pawn(level, initialOffset) {
        var initialPoint;
        initialPoint = new Point(Const.fieldCenterPointX + initialOffset.x,
                                 Const.fieldCenterPointY + initialOffset.y);
        this.turn = {
            frozen: false, // If true, this turn can no longer be changed (has already been played).
            sourcePoint: initialPoint,
            targetPoint: initialPoint
        };
        this.turnList = [this.turn];
        this.level = level;
        this.point = null;
        this.motion = null;
    }

    /**
     * Add a new turn at the end of the object's turn list and freeze
     * the prior turn. The new turn is derived from the prior one.
     */
    Pawn.prototype.appendTurn = function () {
        if (this.turnList.length > 0) {
            this.turnList[this.turnList.length-1].frozen = true;
        }
        this.turnList.push({
            frozen: false,
            sourcePoint: this.turn.targetPoint,
            targetPoint: this.turn.targetPoint
        });
    };

    return Pawn;

});
