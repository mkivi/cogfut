define(function (require) {

    var Kinetic = require('kinetic');

    /**
     * @name Point
     *
     * @class Represents a point in abstract game coordinates. These
     * are translated to and from pixel coordinates on the Kinetic
     * stage for display.
     *
     * @property {number} x
     * @property {number} y
     *
     * @constructor
     * @param {number} x
     * @param {number} y
     */
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Updated dynamically based on the size of the Kinetic stage.
     */
    Point.pixelsPerUnit = null;

    /**
     * Convert pixels to units
     * 
     * @param  {number} pixels 
     * @return {number} units
     */
    Point.unitsFromPixels = function (pixels) {
        return pixels / Point.pixelsPerUnit;
    };

    /**
     * Convert units to pixels
     * 
     * @param  {number} units 
     * @return {number} pixels
     */
    Point.pixelsFromUnits = function (units) {
        return units * Point.pixelsPerUnit;
    };

    /**
     * Convert pixels to Point
     * 
     * @param  {number} xPixels x
     * @param  {number} yPixels y
     * @return {Point}
     */
    Point.fromPixels = function (xPixels, yPixels) {
        return new Point(Point.unitsFromPixels(xPixels),
                         Point.unitsFromPixels(yPixels));
    };

    /**
     * returns the point of the shape
     * 
     * @param  {Kinetic.Image} shape
     * @return {Point}  Point of the shape
     */
    Point.fromShape = function (shape) {
        return Point.fromPixels(shape.getX(), shape.getY());
    };

    /**
     * Makes an array which contains objects of x,y-coordinates in pixels from points
     * 
     * @return {Array} Array of x,y-coordinates objects
     */
    Point.xyPixelArrayFromPoints = function (/* point0, point1, ..., pointN */) {
        var ans, i;
        ans = [];
        for(i = 0; i < arguments.length; i++) {
            if (arguments[i]) {
                ans.push(Point.pixelsFromUnits(arguments[i].x));
                ans.push(Point.pixelsFromUnits(arguments[i].y));
            }
        }
        return ans;
    };

    /**
     * Adds 2 points together
     * 
     * @param {Point} point0 
     * @param {Point} point1 
     * @return {Point} result
     */
    Point.addPoints = function (point0, point1) {
        return new Point(point0.x + point1.x,
                         point0.y + point1.y);
    };

    /**
     * Subtraction point0-point1
     * 
     * @param  {Point} point0 minuend
     * @param  {Point} point1 subtrahend
     * @return {Point} result
     */
    Point.subtract = function (point0, point1) {
        return new Point(point0.x - point1.x,
                         point0.y - point1.y);
    };

    /**
     * Returns the dot product of two points interpreted as vectors.
     * 
     * @param  {Point} point0
     * @param  {Point} point1
     * @return {number} dot product
     */
    Point.dot = function (point0, point1) {
        return (point0.x*point1.x +
                point0.y*point1.y);
    };

    /**
     * Distance between 2 points. Returns the length of a straight line 
     * between two points. The result is always nonnegative.
     * 
     * @param  {Point} point0 
     * @param  {Point} point1
     * @return {Point} distance in units
     */
    Point.distanceBetween = function (point0, point1) {
        var dx, dy;
        dx = point1.x - point0.x;
        dy = point1.y - point0.y;
        return Math.sqrt(dx*dx + dy*dy); // Pythagoras' theorem
    };

    /**
     * Calculates an angle between 2 points. 
     * Returns the angle in radians between two points.
     * 
     * @param  {Point} point0 
     * @param  {Point} point1 
     * @return {float} angle in radians
     */
    Point.angleBetween = function (point0, point1) {
        return Math.atan2(point1.y - point0.y,
                          point1.x - point0.x);
    };

    /**
     * Returns the point you get when you walk n units from this point
     * in the direction indicated by angle (in radians).
     * 
     * @param  {float} angle
     * @param  {number} n in units
     * @return {Point} new point
     */
    Point.prototype.nUnitsInDirection = function (angle, n) {
        return new Point(this.x + n*Math.cos(angle),
                         this.y + n*Math.sin(angle));
    };

    /**
     * Returns the point you get when you walk n units from this point
     * toward targetPoint.
     * 
     * @param  {Point} targetPoint
     * @param  {number} n in units
     * @return {Point} new target point
     */
    Point.prototype.nUnitsToward = function (targetPoint, n) {
        return this.nUnitsInDirection(Point.angleBetween(this, targetPoint), n);
    };

    /**
     * Assigns this point to the shape
     *     
     * @param  {Kinetic.Image} shape
     */
    Point.prototype.assignToShape = function (shape) {
        shape.setPosition(Point.pixelsFromUnits(this.x),
                          Point.pixelsFromUnits(this.y));
    };

    /**
     * Clones this point
     * 
     * @return {Point}
     */
    Point.prototype.clone = function () {
        return new Point(this.x, this.y);
    };

    /**
     * Checks if the points are the same
     *     
     * @param  {Point} other
     * @return {Boolean} true if other and this are the same
     */
    Point.prototype.equals = function (other) {
        // If the x and y distances between two points are both less
        // than this value, the points are considered equal. This
        // can't be in the Const class because that would introduce a
        // circular dependency between Const and Point.
        var pointUnitEpsilon = 0.01;
        return ((Math.abs(other.x - this.x) < pointUnitEpsilon) &&
                (Math.abs(other.y - this.y) < pointUnitEpsilon));
    };

    return Point;

});
