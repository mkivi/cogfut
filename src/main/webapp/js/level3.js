define(function (require) {

    var AIPawn     = require('aipawn');
    var Const      = require('const');
    var Level      = require('level');
    var Pawn       = require('pawn');
    var PlayerPawn = require('playerpawn');
    var Point      = require('point');

    /**
     * @name Level3
     *
     * @class Implements level three of the game. There are two player
     * pawns and one opponent. The rules have not been specified yet.
     */
    function Level3(cogfut) {
        this.super = this.__proto__.__proto__;
        this.super.constructor.call(this, cogfut);
    };

    Level3.prototype.__proto__ = Level.prototype;

    /**
     * Make the gamepieces and rules for this level.
     */
    Level3.prototype.setupLevel = function () {
        this.pawn1 = new PlayerPawn(this, new Point(-5, -15));
        this.pawn2 = new PlayerPawn(this, new Point(+5, -15));
        this.pawnP = new AIPawn(this, new Point(0, -40));
    }

    return Level3;

});
